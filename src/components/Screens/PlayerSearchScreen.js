import React from 'react';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import firebase from 'firebase';
import * as firestore from 'firebase/firestore';
import { Text, View, ScrollView, TouchableOpacity, Picker, TextInput } from 'react-native';
import Card from '../common/Card';
import CardSection from '../common/CardSection';
import AltButton2 from '../common/AltButton2';
import AltPlayerDetail from '../AltPlayerDetail';
import { playerIDstoreAction } from '../../redux/actions';
import InputNoLabel from '../common/InputNoLabel';
import Loading from './Loading';
import PlayerScreen from './PlayerScreen';

/**
This component returns a screen that allows users to search for specific players.
It is the player browsing interface.
*/

class PlayerSearchScreen extends React.Component {

  /**
  author: Elliot Gordon

  The constructor initialises the state object and retrieves props from
  parent components.
  */

  constructor(props) {
    super(props);
    this.state = {
      playerArray: [],
      loading: true,
      //team, role, none, both
      filter: 'team',
      teamFilter: 'none',
      roleFilter: null,
      shownPlayerArray: [],
      searchInput: '',
      searching: false,
      found: false,
      searched: false,
      foundPlayer: null
    }

    const { viewStyle } = styles;

  }

  /**
  When this component mounts, it retrieves all player information stored in the
  database and stores it in playerArray and shownPlayerArray in state.
  */

  async componentDidMount() {
    this.ref = firebase.firestore().collection('players');
    await this.ref.get()
    .then(x => {
      //adding values to be rendered from state
      x.forEach(doc => {
        const { id, name, team, role } = doc.data();

        if (id === -1) {

        } else {

        this.state.playerArray.push({
          id: id,
          name: name,
          team: team,
          role: role,
          primaryColor: doc.data().primaryColor,
          secondaryColor: doc.data().secondaryColor
        });
      }
      });

    //  console.log(this.state.playerArray);
      this.setState({
        loading: false,
        shownPlayerArray: this.state.playerArray
      });
    });
  }

  /**
  This clears the playerArray field.
  */

  componentWillUnmount() {
    this.setState({
      playerArray: []
    });
  }

  /**
  The render helper dictates what is shown on the screen, including showing the
  loading screen while data is being retrieved.
  */

  renderHelper() {
    if (this.state.loading) {
      return (<Loading message='Loading players...' />);
    } else if (!this.state.searching){
      return (
        <Card
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center' }}
        >
        <ScrollView>



        <CardSection style={{ borderColor: '#fff'}}>
        <Picker
      selectedValue={this.state.teamFilter}
      onValueChange={(value) => {
        this.onFilterApply(value);
      }}
      style={{ flex: 1 }}
        >
        <Picker.Item label='Select a team' value = 'none' />
        <Picker.Item label='Atlanta Reign' value='ATL' />
        <Picker.Item label='Boston Uprising' value='BOS' />
        <Picker.Item label='Chengdu Hunters' value='CDH' />
        <Picker.Item label='Dallas Fuel' value='DAL' />
        <Picker.Item label='Florida Mayhem' value='FLA' />
        <Picker.Item label='Guangzhou Charge' value='GZC' />
        <Picker.Item label='Houston Outlaws' value='HOU' />
        <Picker.Item label='Hangzhou Spark' value='HZS' />
        <Picker.Item label='London Spitfire' value='LDN' />
        <Picker.Item label='Los Angeles Gladiators' value='GLA' />
        <Picker.Item label='Los Angeles Valiant' value='VAL' />
        <Picker.Item label='New York Excelsior' value='NYE' />
        <Picker.Item label='Paris Eternal' value='PAR' />
        <Picker.Item label='Philadelphia Fusion' value='PHI' />
        <Picker.Item label='San Francisco Shock' value='SFS' />
        <Picker.Item label='Seoul Dynasty' value='SEO' />
        <Picker.Item label='Shanghai Dragons' value='SHD' />
        <Picker.Item label='Toronto Defiant' value='TOR' />
        <Picker.Item label='Vancouver Titans' value='VAN' />
        <Picker.Item label='Washington Justice' value='WAS' />

      </Picker>
        </CardSection>
          <View style={{alignItems: 'center', backgroundColor: '#fff'}}>
            <AltButton2 onPress={() => this.setState({ searching: true })}> Search by name </AltButton2>
          </View>

        <CardSection style={{borderColor: '#fff'}}>
        <View style={{
          alignItems: 'center',
          flex: 1
        }}>
          <Text style={{
            fontSize: 20,
            color: '#e18629',
            padding: 10,
            fontWeight: 'bold'
          }}>Click to view a player</Text>
        </View>
        </CardSection>
        {this.renderPlayers()}
        </ScrollView>
              </Card>
      );
    } else if (this.state.searching) {
      return (
        <Card>

        <CardSection style={{
          borderColor:'#fff'
        }}>
        <View style={{
          alignItems: 'center',
          flex:1,
          height: 70,
          backgroundColor: '#fff'
        }}>
          <Text style={{
            fontWeight: 'bold',
            fontSize: 20,
            color: '#e18629',
            padding: 5
          }}>Search for a player by name</Text>
        <InputNoLabel
        placeholder='Enter a player name'
        onChangeText={(value) => this.setState({ searchInput: value})}
        value={this.state.searchInput}
        />
        </View>

        </CardSection>
        <View>
          <View style={{
            alignItems: 'center',
            backgroundColor: '#fff',
            padding: 10
          }}>
            <AltButton2  onPress={() => this.search(this.state.searchInput)}> Search </AltButton2>
          </View>
        </View>
        <View style={{
          backgroundColor: '#fff',
          alignSelf: 'stretch',
          height: 600
        }}>
        {this.showSearch()}
        <View style={{
          alignItems: 'center',
          padding: 10
        }}>
          <AltButton2 onPress={() => this.setState({
            searching: false,
            searched: false,
            found: false,
            foundPlayer: null
          })}>BACK</AltButton2>
        </View>
        </View>
        </Card>
      );
    }
  }

  /**
  This is executed when the user presses the search button. It takes the input
  and checks to see if it matches any player names. It passes the result into state.
  */

  search(searchInput) {
    for (let i = 0; i < this.state.playerArray.length; i++) {
      //console.log(this.state.playerArray[i].name);
      //console.log(searchInput)
      const convertedName = this.state.playerArray[i].name.toLowerCase();
      const convertedInput = searchInput.toLowerCase();
      if (convertedName === convertedInput) {
        this.setState({
          searched: true,
          found: true,
          foundPlayer: this.state.playerArray[i]
        });
        /*return (

        );*/
        return;
      }
    }

    this.setState({
      searched: true,
      found: false
    });
  }

  /**
  This method returns either an empty space (when the component mounts), a
  player, or text saying the player couldn't be found. It handles the search results.
  */

  showSearch() {
    if (this.state.searched && this.state.found) {
      const player = this.state.foundPlayer;
      return (

        <TouchableOpacity
        key={player.id}
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          backgroundColor: '#' + `${player.primaryColor}`,
          height: 50,
          borderWidth: 1,
          borderColor: '#e18629',
          borderRadius: 5,
          alignItems: 'center'
        }}
        onPress={() => {
          this.props.playerIDstoreAction(player.id);
          this.playerScreenOrPlayerScreen2();
        }}
        >
          <Text style={{
            color: '#' + `${player.secondaryColor}`,
            fontSize: 20,
            fontWeight: 'bold',
            flex:2,
            paddingLeft: 20
          }}>{player.name}</Text>

          <Text style={{
            textTransform: 'uppercase',
            color: '#' + `${player.secondaryColor}`,
            flex:1,
            fontWeight: 'bold',
            alignSelf: 'center'

          }}>{player.role}</Text>

        <Text style={{
          color: '#' + `${player.secondaryColor}`,
          flex:1,
          fontWeight: 'bold',
          alignSelf: 'center',
          paddingLeft: 40
        }}>{player.team}</Text>
        </TouchableOpacity>
      );
    } else if (this.state.searched && !this.state.found) {
      return (
        <View style={{
          alignItems: 'center'
        }}>
        <Text style={{
          fontWeight: 'bold',
          fontSize: 16,
          color: '#e18629',
          padding: 5
        }}>Sorry, we didn't recognise that name.</Text>
        </View>
      );
    } else if (!this.state.searched) {
      return (
        <View />
      );
    }
  }

  /**
  This method differentiates between two instances of the same component which
  are separated for routing purposes. If this isn't here, trying to access the screen
  will fail.
  */

  playerScreenOrPlayerScreen2() {
    //check for flow
    if(this.props.routerFlow === 'mainFlow') {
      Actions.playerScreen();
    } else if (this.props.routerFlow === 'LDRFlow') {
      Actions.playerScreen2();
    }
  }

  /**
  This method takes the players in the shownPlayerArray and maps each to a block
  showing their information.
  */

  renderPlayers() {
    return this.state.shownPlayerArray.map(player =>
      <View key={player.id}>

      <TouchableOpacity
      style={{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: '#' + `${player.primaryColor}`,
        height: 50,
        borderWidth: 2,
        borderColor: '#e18629',
        borderRadius: 5,
        alignItems: 'center'
      }}
      onPress={() => {
        this.props.playerIDstoreAction(player.id);
        this.playerScreenOrPlayerScreen2();
      }}
      >
        <Text style={{
          color: '#' + `${player.secondaryColor}`,
          fontSize: 20,
          fontWeight: 'bold',
          flex:2,
          paddingLeft: 20
        }}>{player.name}</Text>

        <Text style={{
          textTransform: 'uppercase',
          color: '#' + `${player.secondaryColor}`,
          flex:1,
          fontWeight: 'bold',
          alignSelf: 'center'

        }}>{player.role}</Text>

      <Text style={{
        color: '#' + `${player.secondaryColor}`,
        flex:1,
        fontWeight: 'bold',
        alignSelf: 'center',
        paddingLeft: 40
      }}>{player.team}</Text>
      </TouchableOpacity>
      <View style={{height: 5, backgroundColor: '#fff'}} />
      </View>
    );
  }

  /**
  This method is called when a filter is selected. It passes the team specified
  in the filter into state, before triggering the rendering of the new results.
  */

  async onFilterApply(value) {
    console.log(value);
    await this.setState({ teamFilter: value });
    this.renderFilter();
  }

  /**
  This method takes a copy of the playerArray from state, applies the filter to
  it, and replaces the shownPlayerArray with this new filtered array. This allows
  the original playerArray to be preserved, so that the array doesn't need to be
  fetched again.
  */

  filterOnTeam(selectedTeam) {
    if (selectedTeam !== 'none'){
    const newArray =[];
    for (let i = 0; i < this.state.playerArray.length; i++) {
      if (this.state.playerArray[i].team === selectedTeam) {
        newArray.push(this.state.playerArray[i]);
      }
    }
    this.setState({
      shownPlayerArray: newArray
    });
    }
  }

  /**
  This is a sort of middle-man function that differentiates between selecting a
  new filter, and removing the filter, as the process is different. It calls the
  appropriate function.
  */

  renderFilter() {
    if (this.state.teamFilter === 'none') {
        this.renderPlayers();
    } else if (this.state.teamFilter !== 'none') {
        this.filterOnTeam(`${this.state.teamFilter}`);
    }
  }

  /**
  This method dictates what is displayed on the screen.
  */

render() {
  return (
    <View>
    {this.renderHelper()}
    </View>
    );
  }
}

const styles = {
  viewStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: 'white',
    height: 50,
    borderWidth: 1,
    borderColor: '#e18629',
    borderRadius: 5,
    paddingTop: 15

  }
};

/**
The mapStateToProps object is used to attach variables from application state
(the Redux store) to this component as an object called props.
*/

const mapStateToProps = state => {
  return {
    playerID: state.playerKey.playerID,
    routerFlow: state.userKey.routerFlow
  };
};

/**
Exporting the component for use in other components.
*/

export default connect(mapStateToProps, { playerIDstoreAction })(PlayerSearchScreen);
