import React from 'react';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import firebase from 'firebase';
import * as firestore from 'firebase/firestore';
import { Text, View, ScrollView, Image } from 'react-native';
import Card from '../common/Card';
import CardSection from '../common/CardSection';
import Button from '../common/Button';
import Loading from './Loading';

/**
This component shows users the overwatch league matches scheduled for the
current week.
*/

class Schedule extends React.Component {

  /**
  author: Elliot Gordon

  The constructor initialises the state object and retrieves props from
  parent components.
  */

  constructor(props){
    super(props);
    this.state = {
      loading: true,
      nextMatchDate: null,
      team1: '',
      team2: '',
      logo1: null,
      logo2: null,
      currentWeek: null,
      currentStage: null,
      currentStageName: null,
      currentWeekName: null,
      weekMatches: null,
      leagueActive: false
    }
  }

  /**
  When the component mounts, it retrieves all schedule data from the OWL API and
  searches through it to find the current week's matches, which are stored to weekMatches.
  If no data is found for the week, leagueActive stays false, else it's set to true.
  */

  async componentDidMount() {

    await fetch('https://api.overwatchleague.com/schedule')
    .then((result2) => result2.json())
    .then((result2JSON) => {
      const weeks = [];
      const now = new Date();
      console.log(result2JSON.data.stages[0].name);
      const date = new Date(result2JSON.data.stages[0].weeks[0].startDate);
      console.log(date.toString())
      for (let i = 0; i < result2JSON.data.stages.length-2; i++) {
        console.log('got here');
        const stageStart = new Date(result2JSON.data.stages[i].weeks[0].startDate);
        const stageEnd = new Date(result2JSON.data.stages[i].weeks[result2JSON.data.stages[i].weeks.length-1].endDate);
        console.log('stagestart' + stageStart)
        console.log('now' + now)
        console.log('stageend' + stageEnd)

        if ((now >= stageStart) && (now <= stageEnd)) {
          console.log('was true on :' + i)
          //finding current week
          for (let j = 0; j < result2JSON.data.stages[i].weeks.length; j++) {
            if (now <= result2JSON.data.stages[i].weeks[j].endDate && now >= result2JSON.data.stages[i].weeks[j].startDate) {
              console.log('reached here')
              this.setState({
                 currentStage: i,
                 currentStageName: result2JSON.data.stages[i].name,
                 currentWeek: j,
                 currentWeekName: result2JSON.data.stages[i].weeks[j].name,
                 weekMatches: result2JSON.data.stages[i].weeks[j].matches,
                 leagueActive: true
               });
            }
          }

        }
        else {
          console.log('was false on ' + i)
        }
      }
      this.setState({
        loading: false
      });
      if (this.state.currentWeek !== null) {
        console.log(this.state.currentStage);
        console.log(this.state.currentStageName);
        console.log(this.state.currentWeek);
        console.log(this.state.currentWeekName);
      }
    })
  }

  /**
  This method takes the match data in state and turns each match into
  a block displaying the teams and match date and time.
  */

  renderThisWeek() {
    if (this.state.currentWeek !== null) {
      return this.state.weekMatches.map(match =>
        <CardSection key={match.id} style={{
          borderColor: '#fff'
        }}>
        <View style={{height:10}} />
        <View style={{
          flex:1,
          flexDirection: 'row',
          alignItems: 'center',
          borderColor: '#facc82',
          borderRadius: 5,
          borderWidth: 2,
          padding: 5
        }}>
        <View style={{
          flex:2,
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center'
        }}>
          <Text style={styles.tStyle}>{match.competitors[0].name}</Text>
          <Text style={{flex:1}}> VS </Text>
          <Text style={styles.tStyle}>{match.competitors[1].name}</Text>
        </View>
          <View style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'space-around'
          }}>
            <Text style={styles.dateStyle}>{(new Date(match.startDateTS)).toString().substring(0,10)}</Text>
            <Text style={styles.dateStyle}>{(new Date(match.startDateTS)).toString().substring(16,21)}</Text>
          </View>
        </View>

        </CardSection>
      )
    } else {
      return (
        <CardSection>
          <Text>No matches this week</Text>
        </CardSection>
      );
    }
  }

  /**
  The render helper dictates what is shown on the screen, including showing the
  loading screen while data is being retrieved.
  */

  renderHelper() {
    if (this.state.loading) {
      return(
        <Loading message='Loading calendar...' />
      );
    } else if (this.state.leagueActive) {
      return (
        <View style={{ alignSelf: 'stretch', flex: 1, backgroundColor: 'fff' }}>
        <View style={{height:5, backgroundColor: '#fff'}} />
          {this.renderThisWeek()}
        </View>
      );
    } else if (!this.state.leagueActive) {
      return (
        <Card style={{ alignSelf: 'stretch', flex: 1, backgroundColor: 'white' }}>
        <CardSection style={{alignItems: 'center'}}>
          <Text>The league is not currently active</Text>
        </CardSection>
        </Card>
      );
    }
  }

  /**
  This screen dictates what the screen shows.
  */

  render() {
    return (
      <View>
      <ScrollView>
      {this.renderHelper()}
      </ScrollView>
      </View>
    );
  }

}

const styles = {
  tStyle: {
    fontSize: 18,
    color: '#e18629'
  },
  dateStyle: {
    fontSize: 17

  }
};

/**
Exporting the component for use in other components.
*/

export default Schedule;
