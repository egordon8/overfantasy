import React from 'react';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import firebase from 'firebase';
import * as firestore from 'firebase/firestore';
import { Text, View, TouchableOpacity, ScrollView } from 'react-native';
import Card from '../common/Card';
import CardSection from '../common/CardSection';
import Button from '../common/Button';
import AltButton2 from '../common/AltButton2';
import Loading from './Loading';
import {
  playerIDstoreAction,
  targetUserAction,
  setTradeStageAction,
  setOfferedPlayerAction,
  setRequestedPlayerAction,
  storeUserBackupAction
} from '../../redux/actions';

/**
author: Elliot Gordon

This component lets users view other user's teams, and allows users to field
and bench their own players. It's also used in selecting players during trade
proposal.
*/

class TeamScreen extends React.Component {

  /**
  The constructor initialises the state object and retrieves props from
  parent components.
  */

  constructor(props){
    super(props);
    this.state = {
      loading: true,
      userTeam: [],
      stateUsername: '',
      teamSize: null,
      commitText: 'COMMIT'
    }

    //userdata is: score, scoreLastUpdated, team (array), teamSize, username

    this.ref = firebase.firestore()
    .collection('leagues')
    .doc(`${this.props.selectedLeague}`)
    .collection('owners')
    .doc(`${this.props.targetedUserID}`)
  }

  /**
  This method, when the component mounts, retrieves the specified user's team
  from the database. It also establishes how many players are starting (fielded).
  */

  async componentDidMount() {

   await this.ref.get()
    .then(doc => {
      const { username, team } = doc.data();
        this.setState({
          stateUsername: username
        });
      for (let i = 0; i < team.length; i++) {
        if (team[i] !== null) {
          this.state.userTeam.push(team[i]);
          if (team[i].starting) {
            this.setState({
              teamSize: this.state.teamSize + 1
            });
          }
        }
      }
    });
    this.setState({ loading: false });
  }

  /**
  The render helper dictates what is shown on the screen, including showing the
  loading screen while data is being retrieved.
  */

  renderHelper() {
    if (this.state.loading) {
      return (
        <Loading message='Loading team...' />
      );
    } else if ((this.props.tradeView) && this.props.tradeStage === 1) {
      return (
        <Card
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center' }}
        >

        <CardSection style={{ paddingTop: 50, paddingBottom: 50, borderColor:'#fff' }} >

        <View style={{
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#fff',
          flexDirection: 'column',
          width: 340
        }}>

        <View style={{
          alignItems: 'center',
          backgroundColor:'#fff',
          justifyContent: 'center'
        }}>
          <Text style={{
          alignSelf: 'center',
          fontSize: 25,
          color: '#e18629'
        }}>Trading with: {this.state.stateUsername}</Text>
        </View>

        <View style={{height: 15, backgroundColor:'#fff'}} />

        <View style={{
          alignItems: 'center',
          backgroundColor:'#fff',
          justifyContent: 'center'
        }}>
          <Text style={{
          alignSelf: 'center',
          fontSize: 25,
          color: '#e18629'
        }}>Select the player you want</Text>
        </View>

        </View>

        </CardSection>
        {this.tradeRenderOtherUser()}
        </Card>
      );
    } else if ((this.props.tradeView) && this.props.tradeStage === 2) {
      return (
        <Card
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center' }}
        >

        <CardSection style={{ paddingTop: 50, paddingBottom: 50 }} >
        <View style={{
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#fff',
          flexDirection: 'column',
          width: 340
        }}>

        <View style={{
          alignItems: 'center',
          backgroundColor:'#fff',
          justifyContent: 'center'
        }}>
          <Text style={{
          alignSelf: 'center',
          fontSize: 25,
          color: '#e18629'
        }}>Trading for: {this.props.requestedPlayer.name}</Text>
        </View>

        <View style={{height: 15, backgroundColor:'#fff'}} />

        <View style={{
          alignItems: 'center',
          backgroundColor:'#fff',
          justifyContent: 'center'
        }}>
          <Text style={{
          alignSelf: 'center',
          fontSize: 25,
          color: '#e18629'
        }}>Select a player to offer</Text>
        </View>

        </View>
        </CardSection>
        {this.tradeRenderOtherUser()}
        </Card>
      );
    } else if ((this.props.username === this.props.targetedUserID) && (this.props.viewOnly === false)) {
      return (
        <Card
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center' }}
        >
        <View style={{height:15, backgroundColor: '#fff'}} tag='spacer' />

        <View style={{
          alignItems :'center',
          justifyContent: 'center',
          backgroundColor:'#fff'
        }}>
        <AltButton2
        style={{
          width: 120
        }}
        onPress={() => this.commitChanges()}>{this.state.commitText}</AltButton2>
        </View>

        <View style={{
          alignItems: 'center',
          justifyContent: 'center',
          height: 45,
          backgroundColor: '#fff',
          padding: 5
        }}>
        <Text style={{
          fontSize: 30,
          color:'#e18629',
          fontWeight: '500',
          paddingTop: 10
        }}>Your Team</Text>

        <View style={{height:25, backgroundColor: '#fff'}} tag='spacer' />

        </View>
          {this.renderPlayers()}
        </Card>
      );
    } else if ((this.props.username === this.props.targetedUserID) && this.props.viewOnly) {
      return (
        <Card
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center' }}
        >
        <View style={{
          alignItems: 'center',
          justifyContent: 'center',
          height: 50,
          backgroundColor: '#fff',
          padding: 5
        }}>
        <Text style={{
          fontSize: 30,
          color:'#e18629',
          fontWeight: '500',
          paddingTop: 10
        }}>Your Team</Text>
        </View>
        <View style={{height:25, backgroundColor: '#fff'}} tag='spacer' />
          {this.renderPlayersViewOnly()}
        </Card>
      )
    } else if ((this.props.username !== this.props.targetedUserID)) {
      return (
        <Card
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center' }}
        >
        <View style={{
          alignItems: 'center',
          justifyContent: 'center',
          height: 50,
          backgroundColor: '#fff',
          padding: 5
        }}>
        <Text style={{
          fontSize: 30,
          color:'#e18629',
          fontWeight: '500',
          paddingTop: 10
        }}>{this.state.stateUsername}'s Team</Text>
        </View>
        <View style={{height:25, backgroundColor: '#fff'}} tag='spacer' />
          {this.renderPlayersViewOnly()}
        </Card>
      )
    } else {
      return (
        <View>
        <Loading message='Loading' />
        </View>
      );
    }
  }

  /**
  When a user presses the field or bench buttons, the changes are only made on
  the client, to minimise database writes. This method is associated with the commit
  button, which saves the changes that the user has made to the database.
  */

  async commitChanges() {
    const reference = firebase.firestore().collection('leagues').doc(`${this.props.selectedLeague}`)
    .collection('owners')
    .doc(`${this.props.username}`);

    const arrayToAdd = [];
    arrayToAdd.push(null);

    for (let i = 0; i < this.state.userTeam.length; i++) {
      arrayToAdd.push(this.state.userTeam[i]);
    }

    await reference.set({
      team: arrayToAdd
    }, { merge: true })
    this.setState({
      loading: false,
      commitText: 'COMMITTED!'
     });
  }

  /**
  This method set's a players starting value to true on the client only.
  obj: a player object
  */

  field(obj) {
      for (let i = 0; i < this.state.userTeam.length; i++) {
        if (obj.id === this.state.userTeam[i].id) {
          this.state.userTeam[i].starting = true;
          this.setState({
            teamSize: this.state.teamSize + 1
          });
          console.log('okokok');
        }
      }
      this.setState({
        commitText:'COMMIT'
      });
      console.log(this.state.teamsize);

  }

  /**
  This method sets a player's starting value to false on the client only.
  obj: a player object
  */

  bench(obj) {
      for (let i = 0; i < this.state.userTeam.length; i++) {
        if (obj.id === this.state.userTeam[i].id) {
          this.state.userTeam[i].starting = false;
          this.setState({
            teamSize: this.state.teamSize - 1
          });
        }
      }
      this.setState({
        commitText:'COMMIT'
      });
      console.log(this.state.teamsize);
  }

  /**
  This method returns a player's healing or damaging information depending on
  whether they are of the support role or offense/tank respectively.
  player: the player who is being displayed, player object
  */

    supportOrOffense(player) {
      if (player.role === 'support') {
        return (
          <View style={styles.viewStyle}>
            <Text style={styles.tStyle}>
              HP/10
            </Text>
            <Text style={styles.smallTStyle}>
              {(player.stats.healing_avg_per_10m).toFixed(2)}
            </Text>
          </View>
        );
      } else {
        return (
          <View style={styles.viewStyle}>
            <Text style={styles.tStyle}>
              DMG/10
            </Text>
            <Text style={styles.smallTStyle}>
              {(player.stats.hero_damage_avg_per_10m).toFixed(2)}
            </Text>
          </View>
        );
      }
    }

    /**
    This method displays a red BENCH button for fielded players, and a green
    FIELD button for benched players. If the user is currently fielding six
    players, this method shows a grey FULL non-interactive button.
    obj: the player object
    */

  startingConditional(obj) {
    if (obj.starting) {
      return (
        <Button
        onPress={() => this.bench(obj)}
        style={{ backgroundColor: '#fa4843' }}>
          BENCH</Button>
      );
    } else if (!obj.starting && this.state.teamSize < 6) {
    return (
      <Button
      style={{ backgroundColor: 'green' }}
      onPress={() => {
        this.field(obj);
        console.log('ok happened')
        }
      }
      >
       FIELD </Button>
    );
  } else if (!obj.starting && this.state.teamSize === 6) {
    return (
      <Button
      style={{ backgroundColor: 'grey' }}>
        FULL</Button>
    );
  }
  }

  /**
  This method is attached to a button to be executed when pressed. It stores the
  player's ID for use on the player information screen and routes the user to said
  screen.
  */

  onTouchable(player) {
    this.props.playerIDstoreAction(player.id);
    Actions.playerScreen2();
  };

  /**
  This method takes all elements in the userTeam in state and for each element
  returns a block showing the player's information.
  */

  renderPlayers() {
    return this.state.userTeam.map((player) =>
    <View key={player.id}
    style={{
      backgroundColor:'#fff',
    }}>
    <TouchableOpacity onPress={() => this.onTouchable(player)} sytle={{elevation:3}}>
      <CardSection style={{
        borderColor: '#e18629',
        borderWidth: 4,
        borderRadius: 5,
        elevation: 3,
        borderBottomWidth: 4,
        backgroundColor:'#fff'
      }}>

        <View style={styles.headerContentStyle}>
          <View style={{ flexDirection: 'column', flex:5, alignItems: 'center' , justifyContent: 'space-around', backgroundColor:'#fff'}} tag='holds name and role'>
            <Text style={styles.headerTextStyle}>
              {player.name}
            </Text>
            <Text style={styles.smallTStyle}>
              {player.role.toUpperCase()}
            </Text>
          </View>

          <View tag='holds the stats' style={{
            flexDirection: 'row', flex:10, alignItems: 'center', justifyContent: 'space-around'
          }}>
          <View style={styles.viewStyle}>
            <Text style={styles.tStyle}>
              K/10
            </Text>
            <Text style={styles.smallTStyle}>
              {(player.stats.eliminations_avg_per_10m).toFixed(2)}
            </Text>
          </View>

          {this.supportOrOffense(player)}

          <View style={styles.viewStyle}>
            <Text style={styles.tStyle}>
              U/10
            </Text>
            <Text style={styles.smallTStyle}>
              {(player.stats.ultimates_earned_avg_per_10m).toFixed(2)}
            </Text>
          </View>

          </View>

          <View tag='holds button' style={{flex:5, alignItems: 'center', justifyContent: 'center'}}>
          {this.startingConditional(player)}
          </View>


        </View>
      </CardSection>
      </TouchableOpacity>
      <View style={{height:10, backgroundColor: '#fff'}} />
    </View>
    )
  }

  /**
  This method is associated with the SELECT button that appears next to players
  during trade proposal while selecting a player to request or offer. For each
  case, it stores the selected player and advances the trade process by incrementing the
  trade stage and causing a new screen to render.
  obj: the player object
  */

  async press(obj) {
    //console.log(obj);
    if (this.props.tradeStage === 1) {
      const playerObject = {
        id: obj.id,
        name: obj.name,
        role: obj.role,
        stats: obj.stats,
        team: obj.team,
        starting: false
      }
      await this.props.setRequestedPlayerAction(playerObject);
      await this.props.targetUserAction(`${this.props.username}`);
      await this.props.storeUserBackupAction(`${this.state.stateUsername}`);
      await this.props.setTradeStageAction(2);
      Actions.teamScreen();
    } else if (this.props.tradeStage === 2) {
      const playerObject = {
        id: obj.id,
        name: obj.name,
        role: obj.role,
        stats: obj.stats,
        team: obj.team,
        starting: false
      }
      await this.props.setOfferedPlayerAction(playerObject);
      await this.props.setTradeStageAction(3);
      Actions.tradeSupport();
    }
  }

  /**
  This method renders players with select buttons during trade proposal. It functions
  as renderPlayers does, but with a select button for each player.
  */

  tradeRenderOtherUser() {

    return this.state.userTeam.map(player =>
      <View key={player.id} style={{
        backgroundColor:'#fff'
      }}>
      <TouchableOpacity onPress={() => this.onTouchable(player)}>
        <CardSection style={{
          borderColor: '#e18629',
          borderWidth: 4,
          borderRadius: 5,
          elevation: 3,
          borderBottomWidth: 4,
          backgroundColor:'#fff'
        }}>

          <View style={styles.headerContentStyle}>
            <View style={{ flexDirection: 'column', flex:7, alignItems: 'center' , justifyContent: 'space-around', backgroundColor:'#fff'}} tag='holds name and role'>
              <Text style={styles.headerTextStyle}>
                {player.name}
              </Text>
              <Text style={styles.smallTStyle}>
                {player.role.toUpperCase()}
              </Text>
            </View>

            <View tag='holds the stats' style={{
              flexDirection: 'row', flex:10, alignItems: 'center', justifyContent: 'space-around'
            }}>
            <View style={styles.viewStyle}>
              <Text style={styles.tStyle}>
                K/10
              </Text>
              <Text style={styles.smallTStyle}>
                {(player.stats.eliminations_avg_per_10m).toFixed(2)}
              </Text>
            </View>

            {this.supportOrOffense(player)}

            <View style={styles.viewStyle}>
              <Text style={styles.tStyle}>
                U/10
              </Text>
              <Text style={styles.smallTStyle}>
                {(player.stats.ultimates_earned_avg_per_10m).toFixed(2)}
              </Text>
            </View>

            </View>

            <View tag='holds button' style={{flex:5, alignItems: 'center', justifyContent: 'center'}}>
            <AltButton2
            style={{
              width: 60
            }}
            onPress={() => this.press(player)}
            >SELECT</AltButton2>
            </View>

          </View>
        </CardSection>
        </TouchableOpacity>
        <View style={{height:10, backgroundColor: '#fff'}} />
      </View>
    )
  }

  /**
  This method renders a user's team but does not allow benching or fielding. Used
  for viewing other user's teams or viewing one's own team in an archived league.
  */

  renderPlayersViewOnly() {

    return this.state.userTeam.map(player =>
      <View key={player.id}
      style={{
        backgroundColor:'#fff',
      }}>
      <TouchableOpacity onPress={() => this.onTouchable(player)} sytle={{elevation:3}}>
        <CardSection style={{
          borderColor: '#e18629',
          borderWidth: 4,
          borderRadius: 5,
          elevation: 3,
          borderBottomWidth: 4,
          backgroundColor:'#purple'
        }}>

          <View style={styles.headerContentStyle}>
            <View style={{ flexDirection: 'column', flex:5, alignItems: 'center' , justifyContent: 'space-around', backgroundColor:'#fff'}} tag='holds name and role'>
              <Text style={styles.headerTextStyle}>
                {player.name}
              </Text>
              <Text style={styles.smallTStyle}>
                {player.role.toUpperCase()}
              </Text>
            </View>

            <View tag='holds the stats' style={{
              flexDirection: 'row', flex:10, alignItems: 'center', justifyContent: 'space-around'
            }}>
            <View style={styles.viewStyle}>
              <Text style={styles.tStyle}>
                K/10
              </Text>
              <Text style={styles.smallTStyle}>
                {(player.stats.eliminations_avg_per_10m).toFixed(2)}
              </Text>
            </View>

            {this.supportOrOffense(player)}

            <View style={styles.viewStyle}>
              <Text style={styles.tStyle}>
                U/10
              </Text>
              <Text style={styles.smallTStyle}>
                {(player.stats.ultimates_earned_avg_per_10m).toFixed(2)}
              </Text>
            </View>

            </View>


          </View>
        </CardSection>
        </TouchableOpacity>
        <View style={{height:10, backgroundColor: '#fff'}} />
      </View>
    )
  }

  /**
  This screen dictates what the screen shows.
  */

  render() {
    return (
      <View>
      <ScrollView>
      {this.renderHelper()}
      </ScrollView>
      </View>
    );
  }
}

const styles = {
  headerContentStyle: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    height: 50,
    width: 340,
    backgroundColor:'#fff'
  },
  headerTextStyle: {
    fontSize: 16,
    fontWeight:'600'
  },
  viewStyle: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  tStyle: {
    fontSize: 15,
    color: '#e18629',
    fontWeight: '500'
  },
  smallTStyle: {
    fontSize: 15
  }
};

/**
The mapStateToProps object is used to attach variables from application state
(the Redux store) to this component as an object called props.
*/

  const mapStateToProps = state => {
    return {
      selectedLeague: state.leagueKey.selectedLeague,
      username: state.auth.username,
      targetedUserID: state.userKey.targetedUserID,
      tradeView: state.userKey.tradeView,
      tradeStage: state.userKey.tradeStage,
      viewOnly: state.leagueKey.viewOnly,
      requestedPlayer: state.userKey.requestedPlayer
    }
  }

  /**
  Exporting the component for use in other components.
  */

export default connect(mapStateToProps, {
  playerIDstoreAction,
  targetUserAction,
  setTradeStageAction,
  setOfferedPlayerAction,
  setRequestedPlayerAction,
  storeUserBackupAction
  })(TeamScreen);
