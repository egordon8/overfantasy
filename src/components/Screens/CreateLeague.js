import React from 'react';
import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import * as firestore from 'firebase/firestore';
import Card from '../common/Card';
import CardSection from '../common/CardSection';
import InputNoLabel from '../common/InputNoLabel';
import { View, Picker, DatePickerAndroid, Text, TimePickerAndroid, ScrollView } from 'react-native';
import Loading from './Loading';
import Button from '../common/Button';
import AltButton2 from '../common/AltButton2';

import {
  leagueSelectedFunction,
  leagueNameSetter
} from '../../redux/actions';


class CreateLeague extends React.Component {

  /**
  author: Elliot Gordon

  The constructor initialises the state object and retrieves props from
  parent components.
  */

constructor(props){
  super(props)

  this.state = {
    name: '',
    type: 'Snake',
    size: '3',
    serial: null,
    loading: true,
    currentSerial: null,
    date: 'No date selected',
    creationAllowed: null,
    latestStart: new Date(),
    currentStage: null,
    bool: false,
    created: false,
    secondLoading: false
  };

      this.unsubscribe = null;

      const fbConfig = {
      apiKey: 'AIzaSyAURxap0Tqw0UfimORv8zBNF6lLmRX5HY0',
      authDomain: 'fir-runningproj.firebaseapp.com',
      databaseURL: 'https://fir-runningproj.firebaseio.com',
      projectId: 'fir-runningproj',
      storageBucket: 'fir-runningproj.appspot.com',
      messagingSenderId: '163775465880',
      appId: '1:163775465880:web:851d5ff4042e6b33'
    };


  if (!firebase.apps.length) {
    firebase.initializeApp(fbConfig);
  }

  this.ref = firebase.firestore().collection('leagues');
}

  /**
  This method acts when the component mounts. It retrieves player and schedule
  data from the OWL API. It uses the schedule data to determine whether or not
  league creation is allowed, and passes the verdict into its state object as
  creationAllowed.
  */

  async componentDidMount() {
    this.unsubscribe = this.ref.doc('SERIAL').onSnapshot(this.onUpdate);

    await fetch('https://api.overwatchleague.com/players')
        .then((response) => response.json())
        .then((responseJson) => {
        //  console.log(responseJson);
          this.setState({
            players: responseJson,
            loading: false
          });
        })
        .catch((error) => {
          console.log('fetching OW stuff went wrong');
          console.error(error);
        });

        await fetch('https://api.overwatchleague.com/schedule')
        .then((result2) => result2.json())
        .then((result2JSON) => {
          const weeks = [];
          const now = new Date();
          //console.log(result2JSON.data.stages[0].name);
          const date = new Date(result2JSON.data.stages[0].weeks[0].startDate);
          //console.log(date.toString())
          for (let i = 0; i < result2JSON.data.stages.length-2; i++) {
            //console.log('got here');
            const stageStart = new Date(result2JSON.data.stages[i].weeks[0].startDate);
            const stageEnd = new Date(result2JSON.data.stages[i].weeks[result2JSON.data.stages[i].weeks.length-1].endDate);
            //console.log(i)
            //console.log('stagestart' + stageStart)
            //console.log('now' + now)
          //  console.log('stageend' + stageEnd)

            if ((now >= stageStart) && (now <= stageEnd)) {
              console.log('cannot start league')
              this.setState({
                creationAllowed: false,
                currentStage: i
              });
            }
          }
          if (this.state.creationAllowed === null) {
            this.setState({
              creationAllowed: true
            });
          }
          //no creation 1 week before stage
          //for each stage
          for (let p = 0; p < result2JSON.data.stages.length; p++) {
            //if not playoffs or grand finals
            if (result2JSON.data.stages[p].slug !== 'playoffs' && result2JSON.data.stages[p].slug !== 'grand-finals') {
              //get start date for stage
              const starts = new Date(result2JSON.data.stages[p].weeks[0].startDate);

              //applies next stage only
              if (now <= starts) {
                //gets date 2 days before start
                if (!this.state.bool){
                  const acceptableDate = new Date(starts - (86400000 * 2));
                  this.setState({
                    latestStart: new Date(acceptableDate - (86400000 * 2)),
                    bool: true
                  });
                  //if less than 2 days before start, do not allow creation
                  if (now >= acceptableDate) {
                    this.setState({
                      creationAllowed: false
                    });
                  }
                }

              }



            }
          }

          //may need to move


        })
  }

  /**
  This method closes the connection to the database.
  */

  componentWillUnmount() {
    this.unsubscribe();
  }

  /**
  This method is called whenever a change is detected in the section of the
  database being listened to. It ensures that only the most recent serial is
  used in league creation.
  */

  onUpdate = (querySnapshot) => {
    const returnedSerial = querySnapshot.data().count;
    this.setState({
      serial: returnedSerial
    });
    this.setState({
      loading: false
    });
  }

  /**
  This method uses the data stored in the state object to initialise a league
  in the database.
  */

  async createLeague() {
    let currentSerial = this.state.serial;

    await this.setState({
      secondLoading: true
    });


    this.props.leagueNameSetter(this.state.name);
    //add league to leagues collection
    await this.ref.doc(`${this.state.serial}`).set({
      name: this.state.name,
      type: this.state.type,
      size: this.state.size,
      creator: this.props.currentUser,
      leagueID: currentSerial,
      draftStatus: 'notDone',
      draftingNow: 0,
      lastUpdated: firebase.firestore.FieldValue.serverTimestamp(),
      owners: [this.props.currentUser],
      ownerUsernames: [this.props.username],
      lastDraftedPlayer: '',
      lastDraftedUser: '',
      numberOfOwners: 1,
      draftDate: this.state.date.toString()
    })
    .then(this.ref.doc('SERIAL')
    //increment serial for the next league that is created
      .update({ count: firebase.firestore.FieldValue.increment(1) }))
      //add league to user's list of leagues
      .then(firebase.firestore()
        .collection('users')
        .doc(`${this.props.currentUser}`)
        .collection('theirLeagues')
        .doc(`${currentSerial}`)
        .set({
          name: this.state.name,
          type: this.state.type,
          leagueID: `${currentSerial}`,
          draftStatus: 'active'
        }))
        .then(
          //add user document to owners sub-collection
          firebase.firestore()
          .collection('leagues')
          .doc(`${currentSerial}`)
          .collection('owners')
          .doc(`${this.props.username}`)
          .set({
            score: 0,
            team: [null],
            teamSize: 0,
            username: `${this.props.username}`
          })
        )
        .then(
          //add user document to trades sub-collection
          await firebase.firestore()
          .collection('leagues')
          .doc(`${currentSerial}`)
          .collection('trades')
          .doc(`${this.props.username}`)
          .set({
            offers: [null],
            proposals: [null]
          })
        )
        .then(
          this.setState({
            loading: true
          }));
          this.props.leagueSelectedFunction(currentSerial);

        //adds the playerArray to the league
        await this.addPlayers(currentSerial);
        await this.setState({
          secondLoading: false
        })
        //return to previous screen
          Actions.pop();
  }

  /**
  This method retrieves player data from the players collection in the database
  and copies it to the league as a playerArray.
  currentSerial: A serial number that will be used so that players get added
    to the right league.
  */

  async addPlayers(currentSerial) {
    console.log('starting to add');
    const players = [];
    /*(for (let i = 0; i < this.state.players.content.length; i++) {
      players.push({
        id: this.state.players.content[i].id,
        name: this.state.players.content[i].name,
        role: this.state.players.content[i].attributes.role });
    }*/
    this.addPRef = firebase.firestore().collection('players');
    await this.addPRef.get().then(x => {
      x.forEach(doc => {
        if (doc.data().id !== -1){
        players.push({
          id: doc.data().id,
          name: doc.data().name,
          role: doc.data().role,
          team: doc.data().team,
          stats: doc.data().stats
        });
      }
      })
    })
    await firebase.firestore().collection('leagues').doc(`${currentSerial}`)
    .set({
      playerArray: players
    //});
    }, { merge: true });
  }

  /**
  This method is used to make the Android date selector pop up so the user
  can select a draft date. It will then call timerSelect(d), passing it d, which
  is the date selected.
  */

  async dateSelect() {
    const d = new Date();
    const f = new Date(d.getDate() + (3 * 846000000))
    //console.log(Date.parse(f));
    this.setState({
      latestStart: f
    });
    //console.log(Date.parse(this.state.latestStart));
    const v = Date.parse(this.state.latestStart) + (3 * 846000000);
    const l = new Date(v);
    console.log(l);
    console.log(new Date())

    const k = this.state.latestStart;
    console.log(k)

    const {action, year, month, day} = await DatePickerAndroid.open({
    date: new Date(),
    minDate: new Date(),
    maxDate: k
  });
  if (action !== DatePickerAndroid.dismissedAction) {
    //const y = new Date().getFullYear();
    const d = new Date(year, month, day, 0, 0, 0);
  //  d.setFullYear(2019);
    /*this.setState({
      date: d.toString()
    });*/
    this.timerSelect(d);
  }
  }

  /**
  This method makes the Android time selector pop up for the user to select a
  draft time. It stores the result, combined with parameter d, the date, to
  the date variable in state.
  */

  async timerSelect(d) {
      const {action, hour, minute} = await TimePickerAndroid.open({
      hour: 14,
      minute: 0,
      is24Hour: true,
    });
    if (action !== TimePickerAndroid.dismissedAction) {
      this.setState({
        date: new Date(d.getFullYear(), d.getMonth(), d.getMonth(), hour, minute, 0, 0)
      })



      //console.log(d.getYear() + '' + d.getMonth()  + '' + d.getDay()  + '' + hour  + '' + minute );
    }
  }

  /**
  This method conditionally renders either the date selector or the selected date
  depending on if a date has been selected.
  */

  showDateSelect() {
    if (this.state.date === 'No date selected') {
      return (
        <View style={{
          alignItems: 'center',
          height: 60,
          paddingTop: 15
        }}>
        <AltButton2 onPress={() => this.dateSelect()} > Select a Draft Date </AltButton2>
        </View>
      );
    } else return (
      <View style={{
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        paddingBottom: 15,
        paddingTop: 10,
        height: 60
      }}>
      <Text style={{
        color: '#e18629',
        fontSize: 20
      }}>Draft scheduled for:</Text>
      <Text style={{
        fontSize: 20
      }}>{this.state.date.toString().substring(0,10)}</Text>
      <Text style={{
        fontSize: 20
      }}>{this.state.date.toString().substring(16,21)}</Text>
      </View>

    );
  }

  /**
  The render helper dictates what is shown on the screen, including showing the
  loading screen while data is being retrieved.
  */

  renderHelper() {
    if(this.state.secondLoading) {
      return(
        <Loading message='Setting up league...' />
      );
    } else if (this.state.loading) {
      return (
        <Loading message='Loading...' />
      );
    } else  if (this.state.creationAllowed){
      return(
        <ScrollView>
        <View style={{
          backgroundColor:'fff',
          flex: 1,
          height:750
        }}>
          <CardSection style={{
            borderWidth: 0,
            borderColor: '#fff'
          }}>
          <View style={{height: 15, backgroundColor: '#fff'}} />
          <View style={{
            alignItems: 'center',
            flex: 1
          }}>
            <View style={{
              alignItems: 'center'
            }} >
              <Text style={{
                fontSize: 20,
                color: '#e18629',
                paddingTop: 10
              }}>Enter a name for your League</Text>
            </View>
            <View style={{height: 10}} />
              <View style={{
                height: 30,
                alignItems: 'center'
              }}>
                <InputNoLabel
                secureTextEntry={false}
                placeholder='example name'
                value={this.state.name}
                onChangeText={(name) => this.setState({ name })} />
              </View>

            </View>

          </CardSection>

          <CardSection style={{
            borderWidth: 0,
            height: 100,
            borderColor: '#fff'
          }}>
          <View style={{
            flex: 1,
            flexDirection: 'column'
          }}>
            <Text style={{
              fontSize: 20,
              color: '#e18629',
              paddingLeft: 90,
              paddingTop: 10
            }}>Select a Draft Type</Text>
          <Picker
        selectedValue={this.state.type}
        onValueChange={(value) => this.setState({ type: value })}
        style={{ width: 125, alignSelf: 'center' }}
          >
          <Picker.Item label='Snake' value='Snake' />
          <Picker.Item label='Autopick' value='Autopick' />
        </Picker>
        </View>

          </CardSection>

          <CardSection style={{
            borderWidth: 0,
            height: 100,
            borderColor: '#fff'

          }}>
          <View style={{
            flex: 1,
            flexDirection: 'column',
            paddingBottom: 10,
            alignItems: 'center'
          }}>
          <Text style={{
            fontSize: 20,
            color: '#e18629',
            paddingTop: 10,
          }}>Select a League Size</Text>
          <Picker
        selectedValue={this.state.size}
        onValueChange={(value) => this.setState({ size: value })}
        style={{ width: 100, alignSelf: 'center' }}
          >
          <Picker.Item label='3' value='3' />
          <Picker.Item label='4' value='4' />
          <Picker.Item label='5' value='5' />
          <Picker.Item label='6' value='6' />
          <Picker.Item label='7' value='7' />
          <Picker.Item label='8' value='8' />
          <Picker.Item label='9' value='9' />
          <Picker.Item label='10' value='10' />
          <Picker.Item label='11' value='11' />
          <Picker.Item label='12' value='12' />
          <Picker.Item label='13' value='13' />
          <Picker.Item label='14' value='14' />
          <Picker.Item label='15' value='15' />
          <Picker.Item label='16' value='16' />


        </Picker>
        </View>

          </CardSection>


          <View style={{
            height: 75
          }}>
          {this.showDateSelect()}
          </View>
          <View style={{height:10}} />
          <View style={{
            alignItems: 'center',
            paddingTop: 15,
            height: 100
          }}>
            <AltButton2
            onPress={this.createLeague.bind(this)}
            disabled={
              !(this.state.name != null &&
                this.state.name != '' &&
              this.state.type != null &&
              this.state.size > 2 &&
              this.state.size < 19 &&
              this.state.date !== 'No date selected' &&
              !this.state.created)
            }
            > CONFIRM </AltButton2>
          </View>

        </View>

        </ScrollView>

      );
    } else if (!this.state.creationAllowed) {
      return (
        <ScrollView >
          <View style={{height:700, backgroundColor:'#fff', flex: 1, justifyContent:'center', alignItems: 'center'}}>
            <View>
            <Text style={{
              paddingRight: 30,
              paddingLeft: 30,
              fontSize: 35,
              color: '#e18629',
              paddingBottom: 40,
              textAlign: 'center'
            }}>League creation is currently closed. Leagues will be open for creation again at the end of this OWL stage.</Text>
            </View>

            <CardSection style={{
              borderWidth: 0,
              borderColor:'#fff'
            }}>
              <AltButton2 onPress={() => this.setState({ creationAllowed: true })}>SHORTCUT</AltButton2>
            </CardSection>
          </View>
        </ScrollView>
      );
    } else {
      return (
        <Loading message='Loading...' />
      );
    }
  }

  /**
  The render method dictates what is shown on the screen.
  */

  render() {
    return (
      <View>
      {this.renderHelper()}
      </View>
    );
  }
}

  const styles = {
    cardSectionStyle:{
      paddingTop: 20,
      paddingBottom: 20,
      backgroundColor:'#fff',
      borderWidth: 0,
      borderColor: '#fff',
      flex:1
    },
    cardSectionStyle2: {
      backgroundColor: '#e18629',
      borderColor: '#e18629',
      position: 'relative',
      alignItems: 'center'
    }
  }

  /**
  The mapStateToProps object is used to attach variables from application state
  (the Redux store) to this component as an object called props.
  */

const mapStateToProps = state => {
  return {
    email: state.auth.email,
    password: state.auth.password,
    error: state.auth.error,
    loading: state.auth.loading,
    user: state.auth.user,
    currentUser: state.auth.currentUser,
    username: state.auth.username,
    selectedLeague: state.leagueKey.selectedLeague
  };
};

/**
Exporting the component for use in other components.
*/

export default connect(mapStateToProps, { leagueSelectedFunction, leagueNameSetter })(CreateLeague);
