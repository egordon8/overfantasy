import React, { Component } from 'react';
import firebase from 'firebase';
import * as firestore from 'firebase/firestore';
import { connect } from 'react-redux';
import { View, Text, ScrollView, ActivityIndicator } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Card from '../common/Card';
import CardSection from '../common/CardSection';
import Button from '../common/Button';
import Background from '../common/Background';
import AltButton from '../common/AltButton';
import AltInput from '../common/AltInput';
import {
  emailChanged,
  passwordChanged,
  loginUser,
  createUser,
  usernameChanged,
  setErrorAction } from '../../redux/actions';

  /**
  author: Elliot Gordon

  This component handles account creation and corresponds to the create
  account page.
  */

class CreateAccount extends Component {

  /**
  The constructor initialises the state object and retrieves props from
  parent components.
  */

  constructor(props) {
    super(props);
    this.state = {
      confirmAccountCreate: null
    }
  }

  /**
  This method saves changes in the email text input field to application state.
  */

  onEmailChange(text) {
    this.props.emailChanged(text);
  }

  /**
  This method saves changes in the username text input field to application state.
  */

  onUsernameChange(text) {
    this.props.usernameChanged(text);
  }

  /**
  This method saves changes in the password text input field to application state.
  */

  onPasswordChange(text) {
    this.props.passwordChanged(text);
  }

  /**
  This process initiates the creation of an account. It passes the user's input
  to the createUser action.
  */

  async onButtonCreateAccount() {
    const ref = firebase.firestore().collection('users');
    const usernameArray = [];

    const { email, password, username } = this.props;
    if ((email === '')
    || password === ''
    || email === null
    || password === null
    || username === null
    || username === ''
    || username === 'Name'
    || username === 'owners'
    || username === 'draftingNow'
    || username === 'playerArray'
    || username === 'type'
    || username === 'name'

  ) {
      return;
    }
    this.setState({
      loading: true
    })

    await ref.get().then(x => {
      x.forEach(account => {
        //console.log(account.data().username);
        if (account.data().username !== null && account.data().username !== 'undefined') {
        //  const o = account.data().username;
          //const p = o.toString();
          //const y = p.toLowerCase();
          const v = account.data().username;
          //console.log(v.toLowerCase())
          usernameArray.push(v);
        }
      })
    });
    for (let i = 0; i < usernameArray.length; i++) {
    //  const UNAConverted = usernameArray[i].toLowerCase();
      const usernameConverted = username.toLowerCase();
      //if (UNAConverted === usernameConverted) {
      if (usernameArray[i] === usernameConverted){
        console.log('Username taken');
        await this.setState({ confirmAccountCreate: false });
        await this.props.setErrorAction('Username taken');
      } else {
        await this.setState({ confirmAccountCreate: true });
      }
    }
    if (!(this.props.error === 'Username taken')) {
      this.props.createUser({ email, password, username });
      console.log('acc would have been done')
    } else {
      console.log('not ODNE');
    }

  }

  /**
  This method conditionally renders an error message.
  */

  renderError() {
    if (this.props.error) {
      return (
        <View style={{ backgroundColor: '#e18629' }}>
          <Text style={styles.errorTextStyle}>
            {this.props.error}
          </Text>
        </View>
      );
    }
  }

  /**
  This method conditionally renders the buttons on the screen.
  */

  renderButton() {
    if (this.props.loading) {
      return (
        <View style={{
          backgroundColor: '#e18629',
          justifyContent: 'center',
          alignItems: 'center',
          paddingTop: 20,
          paddingRight: 160
        }}>
          <ActivityIndicator size='large' color='#fff' />
        </View>
      );
    }
    return (
        <CardSection style={styles.buttonStyle}>
            <AltButton onPress={this.onButtonCreateAccount.bind(this)}>
              CREATE ACCOUNT
            </AltButton>
        </CardSection>
    );
  }

  /**
  This method returns what is displayed on the screen.
  */

  render() {
    return (
      <Background>
      <View style={{height: 70, backgroundColor:'#e18629'}}/>
      <CardSection style={{backgroundColor:'#e18629', height: 100, borderColor: '#e18629', alignItems:'center'}}>
      <Text style={{ color: '#fff', fontSize: 45, paddingRight:18 }}>OVER.FANTASY</Text>
      </CardSection>

      <CardSection style={styles.cardSectionStyle}>
        <Text style={styles.textStyle}>CREATE AN ACCOUNT</Text>
      </CardSection>

      <CardSection style={styles.cardSectionStyle}>
        <AltInput
          label='USERNAME'
          placeholder='example username'
          onChangeText={this.onUsernameChange.bind(this)}
          value={this.props.username}
        />
      </CardSection>

        <CardSection style={styles.cardSectionStyle}>
          <AltInput
            label='EMAIL'
            placeholder='example@email.com'
            onChangeText={this.onEmailChange.bind(this)}
            value={this.props.email}
          />
        </CardSection>

        <CardSection style={styles.cardSectionStyle}>
          <AltInput
            secureTextEntry
            label='PASSWORD'
            placeholder='example password'
            onChangeText={this.onPasswordChange.bind(this)}
            value={this.props.password}
          />
        </CardSection>

        {this.renderError()}

        {this.renderButton()}
        <View style={{height:20}} />
        <CardSection style={styles.buttonStyle}>
            <AltButton onPress={() => {
              this.props.emailChanged('');
              this.props.passwordChanged('');
              this.props.usernameChanged('');
              Actions.pop();
            }}>
              BACK
            </AltButton>
        </CardSection>

      </Background>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: '#b00702',
    paddingRight: 100
  },
  viewStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  cardSectionStyle: {
    backgroundColor: '#e18629',
    borderColor: '#e18629',
    position: 'relative',
    alignItems: 'center'
  },
  textStyle: {
    fontSize: 20,
    color: '#fff',
    paddingBottom: 5,
    paddingRight: 75
  },
  buttonStyle: {
    backgroundColor: '#e18629',
    borderColor: '#e18629',
    position: 'relative',
    alignItems: 'center',
    paddingRight: 75
  }
};

/**
The mapStateToProps object is used to attach variables from application state
(the Redux store) to this component as an object called props.
*/

const mapStateToProps = state => {
  return {
    email: state.auth.email,
    password: state.auth.password,
    error: state.auth.error,
    loading: state.auth.loading,
    username: state.auth.username
  };
};

/**
Exporting the component for use in other components.
*/

export default connect(mapStateToProps, {
  emailChanged,
  passwordChanged,
  loginUser,
  createUser,
  usernameChanged,
  setErrorAction
})(CreateAccount);
