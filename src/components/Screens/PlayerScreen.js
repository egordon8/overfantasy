import React from 'react';
import firebase from 'firebase';
import * as firestore from 'firebase/firestore';
import { connect } from 'react-redux';
import { Text, FlatList, ScrollView, View, Image } from 'react-native';
import Card from '../common/Card';
import CardSection from '../common/CardSection';
import Button from '../common/Button';
import AltPlayerDetail from '../AltPlayerDetail';
import AltButton from '../common/AltButton';
import Loading from './Loading';
import { playerIDstoreAction } from '../../redux/actions';

/**
This component displayed player information including, OWL name, picture,
real name and stats.
*/

class PlayerScreen extends React.Component {

  /**
  The constructor initialises the state object and retrieves props from
  parent components.
  author: Elliot Gordon

  */

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      player: null
    }

    const { imageStyle } = styles;

  }

  /**
  This method takes the player's ID from state and queries the OWL API with it,
  storing the query result as a player object in state.
  */

  componentDidMount() {
    const pid = this.props.playerID;
    return fetch('https://api.overwatchleague.com/players/' + `${pid}` + '?locale=en_US&expand=stats')
        .then((response) => response.json())
        .then((responseJson) => {
        //  console.log(responseJson);
          this.setState({ player: responseJson });
          console.log(responseJson);
          this.setState({ loading: false })
        })
        .catch((error) => {
          console.log('fetching OW stuff went wrong');
          console.error(error);
        });
    }

    /**
    This method clears the player object in component state and in application state.
    */

    componentWillUnmount() {
      this.props.playerIDstoreAction('');
      this.setState({
        player: null
      });
    }

    /**
    The render helper dictates what is shown on the screen, including showing the
    loading screen while data is being retrieved.
    */

    renderHelper() {
      if (this.state.loading) {
        return (<Loading message='Loading player information...'/>);
      } else {
        return (
          <Card
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center' }}
          >
          <ScrollView>
            <View style={{
              flex: 1,
              flexDirection: 'column',
              backgroundColor: '#fff'
            }}>
            <View style={{padding: 10, backgroundColor: '#fff'}}>
            <View style={{
              borderWidth: 10,
              borderRadius: 5,
              borderColor: '#e18629',
              backgroundColor: '#fff'
            }}>
              <Image
              source={{ uri: `${this.state.player.data.player.headshot}` }}
              style={styles.imageStyle} />
            </View>
            </View>
            <View style={{
              alignItems: 'center'
            }}>
              <Text
              style={styles.titleStyle}
              >
              {this.state.player.data.player.name}</Text>

              <View>
                <Text style={styles.textStyle}>Real Name: {this.state.player.data.player.givenName} {this.state.player.data.player.familyName}</Text>
              </View>



              <View>
                <Text style={styles.textStyle}>Role: {(this.state.player.data.player.attributes.role).toUpperCase()}</Text>
              </View>


              </View>
            </View>
            <CardSection style={{ paddingTop: 10, paddingBottom: 10, flexDirection: 'column' }} >

            <View style={{
              alignItems: 'center'
            }}>
              <Text style={styles.titleStyle}>Statistics</Text>
            </View>

            <View style={{
              flex: 1,
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'space-between'
            }}>
            <View style={styles.viewStyle}>
              <Text style={styles.subheadingStyle}>Deaths/10</Text>
              <Text style={styles.textStyle}>{(this.state.player.data.stats.all.deaths_avg_per_10m).toFixed(2)}</Text>
            </View>

            <View style={styles.viewStyle}>
              <Text style={styles.subheadingStyle}>Eliminations/10</Text>
              <Text style={styles.textStyle}>{(this.state.player.data.stats.all.eliminations_avg_per_10m).toFixed(2)}</Text>
              </View>

              <View style={styles.viewStyle}>
              <Text style={styles.subheadingStyle}>Final-Blows/10</Text>
              <Text style={styles.textStyle}>{(this.state.player.data.stats.all.final_blows_avg_per_10m).toFixed(2)}</Text>
              </View>

              <View style={styles.viewStyle}>
              <Text style={styles.subheadingStyle}>Damage/10</Text>
              <Text style={styles.textStyle}>{(this.state.player.data.stats.all.hero_damage_avg_per_10m).toFixed(2)}</Text>
              </View>

              <View style={styles.viewStyle}>
              <Text style={styles.subheadingStyle}>Healing/10</Text>
              <Text style={styles.textStyle}>{(this.state.player.data.stats.all.healing_avg_per_10m).toFixed(2)}</Text>
              </View>

              <View style={styles.viewStyle}>
              <Text style={styles.subheadingStyle}>Ultimates/10</Text>
              <Text style={styles.textStyle}>{(this.state.player.data.stats.all.ultimates_earned_avg_per_10m).toFixed(2)}</Text>
              </View>

            </View>
            </CardSection>
          </ScrollView>
          </Card>
        );
      }
    }

    /**
    This screen dictates what the screen shows.
    */

    render() {
      return (
        <View>
        {this.renderHelper()}
        </View>
      );
    }
  }

  const styles = {
    imageStyle: {
      height: 300,
      flex: 1,
      width: null
    },
    subheadingStyle: {
      fontSize: 20,
      color: '#e18629',
      fontWeight: '500'
    },
    textStyle: {
      fontSize: 20
    },
    titleStyle: {
      fontSize: 30,
      color: '#e18629',
      fontWeight: '500'
    },
    viewStyle: {
      alignItems: 'center',
      padding: 5
    }
  };

  /**
  The mapStateToProps object is used to attach variables from application state
  (the Redux store) to this component as an object called props.
  */

  const mapStateToProps = state => {
    return {
      playerID: state.playerKey.playerID
    };
  };

  /**
  Exporting the component for use in other components.
  */

export default connect(mapStateToProps, {playerIDstoreAction})(PlayerScreen);
