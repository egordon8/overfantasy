import React, { Component } from 'react';
import firebase from 'firebase';
import * as firestore from 'firebase/firestore';
import { connect } from 'react-redux';
import { View, Text, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Card from '../common/Card';
import CardSection from '../common/CardSection';
import Button from '../common/Button';
import UserDetail from './UserDetail';
import Loading from './Loading';
import AltButton2 from '../common/AltButton2';
import { setTradeViewAction, viewOnlyAction } from '../../redux/actions';

/**
This component is the league's main page. It show each user's score, allows
their teams to be viewed, and allows navigation to the trade interface.
*/

class LeaguePage extends React.Component {

  /**
  author: Elliot Gordon

  The constructor initialises the state object and retrieves props from
  parent components.
  */

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      ownerDataArray: [],
      leagueLast: null,
      playerLast: null,
      text: 'default',
      checkingForUpdates: true,
      updatingScores: false
    };
  }

  /**
  This method is called when the component mounts. It goes through several checks
  that help maintain the league's intended lifecycle. Each step is a method,
  each explained in its own description.
  */

  async componentDidMount() {
    /*making sure user doesn't accidentally get taken to version of team screen
    that is meant for trading or for archived leagues
    */
    this.props.setTradeViewAction(false);
    this.props.viewOnlyAction(false);

    await this.checkForFinished();
    await this.checkDraftFill();

    const selectedLeague = this.props.selectedLeague;
    let verdict = null;
    await this.getLastUpdated().then(result => verdict = result);

    await this.helper(verdict);


  }

  /**
  This method checks to see if the league has been filled. If it has not,
  it sets the draftStatus to ready, to force the league to be filled on the
  ready screen.
  */

  async checkDraftFill() {
    this.ref = firebase.firestore().collection('leagues').doc(`${this.props.selectedLeague}`);
    this.ref.get().then(doc => {
      const missing = doc.data().size - doc.data().numberOfOwners;
      const now = new Date();
      const draftDate = new Date(doc.data().draftDate);
      const dStatus = doc.data().draftStatus;
      if (missing > 0 && now >= draftDate &&  dStatus == 'notDone'){
        this.ref.update({
          draftStatus: 'ready'
        });
      }
    })
  }

  /**
  This method checks with the OWL API to see if the OWL season has ended. If it
  has, then the league's draftStatus is set to leagueOver, and the league becomes
  an archived league.
  */

  async checkForFinished() {

    this.ref = firebase.firestore().collection('leagues').doc(`${this.props.selectedLeague}`);

    await fetch('https://api.overwatchleague.com/schedule')
    .then((result) => result.json())
    .then((resultJSON) => {
      const now = new Date();
      const leagueEnd = new Date(resultJSON.data.endDateMS);
      console.log(now >= leagueEnd);
      if(now >= leagueEnd) {

        this.ref.get().then(doc => {
          const batch = firebase.firestore().batch();
          batch.update(this.ref, {
            draftStatus: 'leagueOver'
          });
          const userEmails = doc.data().owners;
          for (let i = 0; i < userEmails.length; i ++) {
            const userRef = firebase.firestore().collection('users').doc(`${userEmails[i]}`).collection('theirLeagues').doc(`${doc.data().leagueID}`);
            console.log(userEmails[i]);
            console.log(doc.data().leagueID)
            batch.set(userRef, {
              draftStatus: 'archived'
            }, {merge: true});
          }
          batch.commit();
        })
      }
    })
  }

  /**
  This method checks with the database to see if the league needs to have its
  user scores updated. If it does, then it returns a true boolean, which will
  be used as a parameter in the helper method to trigger a score update. Else, it
  returns false.
  */

  async getLastUpdated() {
    const selectedLeague = this.props.selectedLeague;

    await firebase.firestore().collection('leagues').doc(`${selectedLeague}`).get()
    .then(doc => {
      const data = doc.data().lastUpdated;
      this.setState({ leagueLast: data });
    });

    await firebase.firestore().collection('players').doc('LAST_UPDATED').get()
    .then(doc => {
      const data = doc.data().date;
      this.setState({ playerLast: data });
    });
    this.setState({ checkingForUpdates: false });

    //has it been an > an hour since last updated
    return ((this.state.playerLast.toDate() - this.state.leagueLast.toDate()) > 3600000);
  }

  /**
  This method passes user information into the state object. If bool is true, it
  calls the updateScores method.
  bool: Represents the boolean value received from the getLastUpdated method.
  */

  async helper(bool) {
    if (bool) {
      this.setState({ updatingScores: true });
    }
    const arr = [];
    const selectedLeague = this.props.selectedLeague;
    this.ref = firebase.firestore()
      .collection('leagues')
      .doc(`${selectedLeague}`)
      .collection('owners');
    this.ref.get()
    .then(x => {
      //adding values to be rendered from state
      x.forEach(doc => {
        const { username, score, team } = doc.data();

        this.state.ownerDataArray.push({
          username: username,
          score: score,
          team: team
        });
      });

      this.setState({ loading: false });

      if (bool) {
        this.updateScores(this.state.ownerDataArray);
      }
    });

  }

  /**
  This method updates the scores of the users in the league based on the
  impact values associated with each user's players where their starting boolean
  is true. So they only gain points for fielded players.
  */

  async updateScores(dataArray) {
    const arr = [];
    for (let p = 0; p < dataArray.length; p++) {
      const array = [];
      arr.push(array);
    }

    for (let i = 0; i < dataArray.length; i++) {
      //getting all users in the league
      arr[i].push(dataArray[i].username)
       for (let j = 1; j < dataArray[i].team.length; j++) {
         //for each user, getting their fielded players
         if(dataArray[i].team[j].starting) {
           arr[i].push(dataArray[i].team[j].id);
         }
       }

    arr[i].push(0);

    for (let g = 1; g < arr[i].length - 1; g++) {
      await firebase.firestore().collection('players').doc(`${arr[i][g]}`).get()
      .then(doc => {
        arr[i][arr[i].length - 1] += doc.data().impact;
      })
    }

   }
   for (let y = 0; y < arr.length; y++) {
     for (let x = 0; x < this.state.ownerDataArray.length; x++) {
       if (arr[y][0] === this.state.ownerDataArray[x].username) {
        this.state.ownerDataArray[x].score += arr[y][arr[y].length - 1];
       }
     }
   }

   const batch = firebase.firestore().batch();
   this.reference0 = firebase.firestore().collection('leagues').doc(`${this.props.selectedLeague}`)
   .collection('owners');
   for (let v = 0; v < this.state.ownerDataArray.length; v++) {
     batch.update(this.reference0.doc(`${this.state.ownerDataArray[v].username}`), {
       score: this.state.ownerDataArray[v].score
     });
   }
   this.reference = firebase.firestore().collection('leagues').doc(`${this.props.selectedLeague}`);
   batch.update(this.reference, {
     lastUpdated: firebase.firestore.FieldValue.serverTimestamp()
   });

  batch.commit();
   this.state.ownerDataArray.sort(this.compareFunction);

   this.setState({ updatingScores: false });
  }

  /**
  This method takes the elements in ownerDataArray and for each returns a
  UserDetail component.
  */

  renderOwners() {
    return this.state.ownerDataArray.map((user, index) =>
      <UserDetail user={user} key={user.username} index={index} bool={false}/>
    );
  }

  /**
  This method conditionally renders space for styling purposes.
  */

  renderPadding() {
    if (this.state.ownerDataArray.length<8) {
      return (
        <View style={{height:400, backgroundColor: '#fff'}} />
      );
    }
  }

  /**
  The render helper dictates what is shown on the screen, including showing the
  loading screen while data is being retrieved.
  */

  renderHelper() {
    if (this.state.loading){
      return (
        <Loading message='Loading league...' />
      );
    } else {
      return (
        <ScrollView>

        <View style={{
          height: 50,
          alignItems:'center',
          justifyContent: 'center',
          backgroundColor: '#fff'
        }}>
          <Text style={{
            fontSize:30,
            color: '#e18629'
          }}>{this.props.leagueName}</Text>
        </View>

        <View style={{
          height: 50,
          alignItems:'center',
          justifyContent: 'center',
          backgroundColor: '#fff'
        }}>
          <AltButton2 onPress={() => Actions.tradeScreen()}>GO TO TRADES</AltButton2>
        </View>

        {this.renderOwners()}

        <View style={{
          height: 50,
          alignItems:'center',
          justifyContent: 'center',
          backgroundColor: '#fff'
        }}>
          <AltButton2 onPress={() => Actions.pop()} > BACK </AltButton2>
        </View>
        {this.renderPadding()}
        </ScrollView>
      );
    }
  }

  renderConditional() {
    if (this.state.checkingForUpdates) {
      return (
        <Text> Checking for updates... </Text>
      );
    }
    if (this.state.updatingScores) {
      return (
        <Text> Updating scores... </Text>
      );
    }
    return (
      <View />
    );
  }

  compareFunction(a, b) {
    return (
      -(a.score - b.score)
    );
  }

  /**
  This screen dictates what the screen shows.
  */

  render() {
    return (
      <View>
        {this.renderHelper()}
      </View>
    );
  }
}

/**
The mapStateToProps object is used to attach variables from application state
(the Redux store) to this component as an object called props.
*/

  const mapStateToProps = state => {
  return {
    currentUser: state.auth.currentUser,
    username: state.auth.username,
    selectedLeague: state.leagueKey.selectedLeague,
    leagueName: state.leagueKey.leagueName
  };
};

/**
Exporting the component for use in other components.
*/

export default connect(mapStateToProps, { setTradeViewAction, viewOnlyAction })(LeaguePage);
