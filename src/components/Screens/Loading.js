import React from 'react';
import { Actions } from 'react-native-router-flux';
import { Text, View, ActivityIndicator } from 'react-native';
import Background from '../common/Background';

/**
This component is a loading screen that is displayed while other components
are still retrieving data or processing commands.
*/

class Loading extends React.Component {

  /**
  author: Elliot Gordon

  The constructor retrieves props from its parent component.
  */

  constructor(props) {
    super(props);
  }

  /**
  This method returns what is displayed on the screen.
  */

  render() {
    return (

        <View style={styles.loadStyle} >
          <ActivityIndicator size={this.props.size || 'large'} color='#e18629' />
          <Text style={styles.tStyle}>{this.props.message}</Text>
        </View>

    );
  }
}

const styles = {
  loadStyle: {
    backgroundColor: '#fff',
    height: 850,
    width: 450,
    paddingRight: 75,
    paddingBottom: 105,
    alignItems: 'center',
    justifyContent: 'center'
  },
  tStyle: {
    fontSize: 20,
    color: '#e18629',
    paddingTop: 20
  }
};

/**
Exports the component for use in other components.
*/

export default Loading;
