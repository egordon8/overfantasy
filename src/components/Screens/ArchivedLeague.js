import React, { Component } from 'react';
import firebase from 'firebase';
import * as firestore from 'firebase/firestore';
import { connect } from 'react-redux';
import { View, Text, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Card from '../common/Card';
import CardSection from '../common/CardSection';
import Button from '../common/Button';
import UserDetail from './UserDetail';
import AltButton2 from '../common/AltButton2';
import Loading from './Loading';
import { viewOnlyAction } from '../../redux/actions';

/**
author: Elliot Gordon

This component is the alternative to the LeaguePage. This is shown to users that
try to view a league that is in the archives. It displays the users in the
league in descending order of points scored, and still allows them to view the
teams they ended the league with. However, if a user views their own team
through the archived league page, they will not be able to field or bench players.
*/

class ArchivedLeague extends React.Component {

  /**
  The constructor initialises the state object and retrieves props from
  parent components.
  */

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      ownerDataArray: [],
      leagueLast: null,
      playerLast: null,
      text: 'default',
      checkingForUpdates: true,
      updatingScores: false,
      winner: null,
      storageArr: []
    };
  }

  /**
  This method sets viewonly to true so that no benching or fielding can take
  place, and calls the helper() method.
  */

  async componentDidMount() {

    this.props.viewOnlyAction(true);

    const selectedLeague = this.props.selectedLeague;

    let verdict = null;

    await this.helper();

  }

  /**
  This method retrieves user data including their names, scores, and teams
  and passes it into component state as the ownerDataArray.
  */

  async helper() {

    const arr = [];
    const selectedLeague = this.props.selectedLeague;
    this.ref = firebase.firestore()
      .collection('leagues')
      .doc(`${selectedLeague}`)
      .collection('owners');
    this.ref.get()
    .then(x => {
      //adding values to be rendered from state
      x.forEach(doc => {
        const { username, score, team } = doc.data();

        this.state.ownerDataArray.push({
          username: username,
          score: score,
          team: team
        });
      });
      this.setState({ loading: false });
    });
  }

  /**
  This method takes the ownerDataArray from component state and returns, for
  each element, a UserDetail component.
  */

  renderOwners() {
    return this.state.ownerDataArray
    .sort((a, b) => a.score < b.score)
    .map((user, index) =>
      <UserDetail user={user} key={user.username} index={index} bool={true}/>
    );
  }

  /**
  The render helper dictates what is shown on the screen, including showing the
  loading screen while data is being retrieved.
  */

  renderHelper() {
    if (this.state.loading){
      return (
        <Loading message='Loading league...' />
      );
    } else {
      return (
        <View style={{backgroundColor:'#fff'}}>
        <ScrollView >
          {this.renderOwners()}
          <View style={{
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor:'#fff',
            paddingTop: 20
          }}>
            <AltButton2 onPress={() => Actions.pop()}>
              BACK
            </AltButton2>
          </View>
          {this.renderPadding()}
            </ScrollView>
        </View>
      );
    }
  }

  /**
  This method creates space for the sake of styling.
  */

  renderPadding() {
    if (this.state.ownerDataArray.length < 4) {
      return(
        <View style={{height:140}} />
      );
    }
  }

  /**
  This method returns what is displayed by the component. It calls the
  renderHelper() to actually choose what to display.
  */

  render() {
    return (
      <View>
      {this.renderHelper()}
      </View>
    );
  }
}

  /**
  The mapStateToProps object is used to attach variables from application state
  (the Redux store) to this component as an object called props.
  */

  const mapStateToProps = state => {
  return {
    currentUser: state.auth.currentUser,
    username: state.auth.username,
    selectedLeague: state.leagueKey.selectedLeague,
    leagueName: state.leagueKey.leagueName
  };
};

/**
Exporting the component for use in other components.
*/

export default connect(mapStateToProps, { viewOnlyAction })(ArchivedLeague);
