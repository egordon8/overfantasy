import React from 'react';
import firebase from 'firebase';
import * as firestore from 'firebase/firestore';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import DraftScreen from '../DraftScreen';
import LeaguePage from './LeaguePage';
import ReadyScreen from './ReadyScreen';
import ArchivedLeague from './ArchivedLeague';
import Loading from './Loading';
import {
  flowSetAction
} from '../../redux/actions';

  /**
  author: Elliot Gordon

  The purpose of this component is to check the status of the league being
  interacted with, and the display the correct screen to the user.
  */

class LeagueDraftRouter extends React.Component {

  /**
  The constructor initialises the state object and retrieves props from
  parent components.
  */

  constructor(props){
    super(props);
    this.state = {
      loading: true,
      draftStatus: 'nada',
      draftDate: null,
      goToReady: false
    }
    this.ref = firebase.firestore()
    .collection('leagues')
    .doc(`${this.props.selectedLeague}`);
    this.unsubscribe = this.ref.onSnapshot(this.onUpdate);

  }

  /**
  This component stops the loading screen from being shown.
  */

  async componentDidMount() {
    this.setState({ loading: false });
  }

  /**
  This method closes the database connection.
  */

  componentWillUnmount() {
    this.unsubscribe();
    this.props.flowSetAction('mainFlow')
  }

  /**
  This method closes the database connection, but lets this action take
  the form of a method.
  */

  unsub() {
    this.unsubscribe();
  }

  /**
  This method is used to keep track of the league's draftStatus at any given
  time, and to re-render the screen when that changes.
  */

  onUpdate = (querySnapshot) => {
    if (!this.props.deletingLeague){
      const result = querySnapshot.data().draftStatus;
      const date = querySnapshot.data().draftDate;
      //console.log('draft status ' + result);
      this.setState({
        draftStatus: result,
        draftDate: date
      });
      const now = new Date();
      const draftDate = new Date(date);
      if ((now >= draftDate) && result === 'notDone') {
        this.setState({
          goToReady: true
        });
      }
    } else {
      this.setState({
        draftStatus: 'notDone',
        draftDate: new Date()
      });
    }
  }

  /**
  The render helper dictates what is shown on the screen, including showing the
  loading screen while data is being retrieved.
  */

  renderHelper() {
    if (this.state.loading) {
      return (
        <Loading message='Loading...' />
      );
    } else if (this.state.draftStatus === 'underway') {
      return (
        <DraftScreen />
      );
    } else if (this.state.draftStatus === 'ready') {
      return (
        <ReadyScreen date={this.state.draftDate} unsub={this.unsub}/>
      );
    } else if (((this.state.draftStatus === 'complete')
    || (this.state.draftStatus === 'notDone')) ){
      return (
        <LeaguePage goToReady={this.state.goToReady}/>
      );
    } else if (this.state.draftStatus === 'leagueOver') {
      return (
        <ArchivedLeague />
      );
    } else {
      return (
        <Loading message='Loading...' />
      );
    }

  }

  /**
  This screen dictates what the screen shows.
  */

  render() {
    return (
      <View>
      {this.renderHelper()}
      </View>
    );
  }

}

/**
The mapStateToProps object is used to attach variables from application state
(the Redux store) to this component as an object called props.
*/

  const mapStateToProps = state => {
    return {
      selectedLeague: state.leagueKey.selectedLeague,
      deletingLeague: state.leagueKey.deletingLeague
    };
  };

  /**
  Exporting the component for use in other components.
  */

export default connect(mapStateToProps, { flowSetAction })(LeagueDraftRouter);
