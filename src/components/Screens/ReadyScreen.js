import React from 'react';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import firebase from 'firebase';
import * as firestore from 'firebase/firestore';
import { Text, View } from 'react-native';
import Card from '../common/Card';
import CardSection from '../common/CardSection';
import Button from '../common/Button';
import AltButton2 from '../common/AltButton2';
import Loading from './Loading';
import {
  deleteLeagueAction
} from '../../redux/actions';

/**
This component is used to convey the status of the league to users before the draft
starts. It will either allow the creator to start the league, tell the non-creator
to wait for the creator to start the league, or tell the user that they must wait until
the draft date. This component also handles filling the league with bots where needed.
*/

class ReadyScreen extends React.Component {

  /**
  author: Elliot Gordon

  The constructor initialises the state object and retrieves props from
  parent components.
  */

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      ready: null,
      full: null,
      isCreator: null,
      missing: 0
    }
  }

  /**
  This method retrieves the league data and performs checks to see if the draft
  date has been reached.
  */

  async componentDidMount() {
    const today = new Date();
    const draftDate = new Date(this.props.date);

    this.reference = firebase.firestore()
    .collection('leagues')
    .doc(`${this.props.selectedLeague}`);

    await this.reference.get().then(doc => {
      const numberOfOwners = doc.data().numberOfOwners;
      const leagueSize = doc.data().size;
      const creator = doc.data().creator;
      const val = numberOfOwners.toString() === leagueSize.toString();

      this.setState({
        full: val,
        isCreator: this.props.currentUser == creator,
        leagueSize: leagueSize,
        missing: (leagueSize-numberOfOwners)
      });
      //console.log(this.state.isCreator);
    })

    if ((draftDate < today || draftDate === today) && this.state.full === false) {
      //fill league with bots
      await this.setState({ ready: 'notFull' });
      this.fillLeague(this.state.missing);
    } else if (draftDate < today || draftDate === today) {
      await this.setState({ ready: true });
    } else {
      await this.setState({ ready: false });

    }
    //console.log(this.state.ready);
    this.setState({ loading: false });
  }

  /**
  This method fills the league with a number of bots equal to numberMissing.
  numberMissing: The number of empty spaces in the league (size - numberOfOwners).
  */

  async fillLeague(numberMissing) {
    console.log(numberMissing);
    const sizeForFill = this.state.leagueSize;
    const batch = firebase.firestore().batch();
    const leaguesRef = firebase.firestore()
        .collection('leagues')
        .doc(`${this.props.selectedLeague}`);
    for (let i = 0; i < numberMissing; i ++) {
      const botName = 'bot' + `${i + 1}`;
      const botEmail = `${botName}` + '@bot.com';

      batch.update(leaguesRef,
        { owners: firebase.firestore.FieldValue.arrayUnion(`${botEmail}`) }
      );

      batch.update(leaguesRef,
        { ownerUsernames: firebase.firestore.FieldValue.arrayUnion(`${botName}`) }
      );

      batch.set(leaguesRef.collection('owners').doc(`${botName}`),
        {
          score: 0,
          team: [null],
          teamSize: 0,
          username: `${botName}`
        }
      )

      batch.set(leaguesRef.collection('trades').doc(`${botName}`),
        {
          proposals: [null],
          offers: [null]
        }
      )

    }
    await batch.commit();

    await leaguesRef.update({
      numberOfOwners: sizeForFill
    });

  }

  /**
  This method starts the draft by setting the draftStatus to underway.
  */

  startDraft() {
    this.ref = firebase.firestore()
    .collection('leagues')
    .doc(`${this.props.selectedLeague}`);

    this.ref.update({
      draftStatus: 'underway'
    });
  }

  /**
  This method determines what message needs to be displayed based on whether or not
  the draft date has been met and whether or not the current user is the creator.
  */

  renderReady() {
    if ((this.state.ready === true) && this.state.isCreator) {
      return(
        <View style={{
          height: 800,
          backgroundColor: '#fff'
        }}>
        <View style={{
          alignItems: 'center',
          paddingTop: 40
        }}>
          <Text style={{
          alignSelf: 'center',
          fontSize: 25,
          color: '#e18629',
          textAlign: 'center'
         }}>Your draft is ready to start!</Text>
        </View>
        <View style={{
          alignItems: 'center',
          paddingTop: 30
        }}>
          <AltButton2 onPress={() => this.startDraft() }>Begin Draft</AltButton2>
        </View>

        <View style={{
          alignItems: 'center',
          paddingTop: 30
        }}>
          <AltButton2 onPress={() => Actions.pop()}>BACK</AltButton2>
        </View>
        </View>
      );
    } else if (this.state.ready === 'notFull' && this.state.isCreator) {
      //should fill with bots
      /*return (
        <View>
        <CardSection>
        <Text> Your league was not filled in time for the draft, so we have filled it with bots. Click below to begin your draft.</Text>
        </CardSection>
        <CardSection>
          <Button onPress={() => this.startDraft() }>Begin Draft</Button>
        </CardSection>
        </View>
      );*/
      return (
        <View style={{
          height: 800,
          backgroundColor: '#fff'
        }}>
        <View style={{
          alignItems: 'center',
          paddingTop: 40
        }}>
          <Text style={{
          alignSelf: 'center',
          fontSize: 25,
          color: '#e18629',
          paddingLeft: 15,
          paddingRight: 15,
          textAlign: 'center'
         }}>Your league was not filled in time for the draft, so we have filled it with bots. Click below to begin your draft.</Text>
        </View>
        <View style={{
          alignItems: 'center',
          paddingTop: 30
        }}>
          <AltButton2 onPress={() => this.startDraft() }>Begin Draft</AltButton2>
        </View>

        <View style={{
          alignItems: 'center',
          paddingTop: 30
        }}>
          <AltButton2 onPress={() => Actions.pop()}>BACK</AltButton2>
        </View>
        </View>
      );
    } else if ((this.state.ready && !this.state.isCreator) || (this.state.ready === 'notFull' && !this.isCreator)) {
      return(
        <View style={{
          height: 800,
          backgroundColor: '#fff'
        }}>
        <View style={{
          alignItems: 'center',
          paddingTop: 40,
          justifyContent: 'center'
        }}>
          <Text style={{
          alignSelf: 'center',
          fontSize: 25,
          color: '#e18629',
          paddingLeft: 15,
          paddingRight: 15,
          textAlign: 'center'
         }}>Draft is ready to start. Waiting for league creator to begin.</Text>
        </View>

        <View style={{
          alignItems: 'center',
          paddingTop: 30
        }}>
          <AltButton2 onPress={() => Actions.pop()}>BACK</AltButton2>
        </View>

        </View>
      );
    }
    else if (this.state.ready === false){
      /*return (
        <View>
        <CardSection>
          <Text>Your draft will be ready to start on:</Text>
        </CardSection>
        <CardSection>
          <Text>{this.props.date.toString()}</Text>
        </CardSection>
        <CardSection>
          <Text>Until then, why don't you have a think about the players you want to draft?</Text>
        </CardSection>
        <CardSection>
          <Button onPress={() => Actions.playerSearch2()}>Browse Players</Button>
        </CardSection>
        </View>
      );*/

      return (
        <View style={{
          height: 800,
          backgroundColor: '#fff'
        }}>

        <View style={{
          alignItems: 'center',
          paddingTop: 40,
          justifyContent: 'center',
        }}>
          <Text style={{
          alignSelf: 'center',
          fontSize: 25,
          color: '#e18629',
          textAlign: 'center'
        }}>Your draft will be ready to start on:</Text>
        </View>

        <View style={{
          alignItems: 'center',
          paddingTop: 20,
          justifyContent: 'center'
        }}>
          <Text style={{
          alignSelf: 'center',
          fontSize: 25,
          color: '#e18629',

        }}>{this.props.date.toString().substring(0,10)} {this.props.date.toString().substring(16,22)}</Text>
        </View>

        <View style={{
          alignItems: 'center',
          paddingTop: 20
        }}>
          <Text style={{
          alignSelf: 'center',
          fontSize: 25,
          color: '#e18629',
          textAlign:'center'
        }}>Until then, why don't you have a think about the players you want to draft?</Text>
        </View>

        <View style={{
          alignItems: 'center',
          paddingTop: 30
        }}>
          <AltButton2 onPress={() => Actions.playerSearch2()}>View Players</AltButton2>
        </View>

        <View style={{
          alignItems: 'center',
          paddingTop: 30
        }}>
          <AltButton2 onPress={() => Actions.pop()}>BACK</AltButton2>
        </View>

        </View>
      );
    }  else {
      return (
        <Loading message='We are some trouble loading...' />
      );
    }
  }

  /**
  This method is used in deleting a league from the database.
  */

  async deleteLeague() {
    await this.props.deleteLeagueAction(true);
    await firebase.firestore()
    .collection('users')
    .doc(`${this.props.currentUser}`)
    .collection('theirLeagues')
    .doc(`${this.props.selectedLeague}`).delete()
    .then(
      firebase.firestore()
      .collection('leagues')
      .doc(`${this.props.selectedLeague}`).delete()
    )
    Actions.pop();
  }

  /**
  The render helper dictates what is shown on the screen, including showing the
  loading screen while data is being retrieved.
  */

  renderHelper() {
    if (this.state.loading) {
      return (
        <Loading message='Loading...' />
      );
    } else {
      return (
        <Card
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center' }}
        >

        {this.renderReady()}


        </Card>
      );
    }
  }

  /**
  This screen dictates what the screen shows.
  */

  render() {
    return (
      <View>
        {this.renderHelper()}
      </View>
    );
  }
}

/**
The mapStateToProps object is used to attach variables from application state
(the Redux store) to this component as an object called props.
*/

  const mapStateToProps = state => {
    return {
      selectedLeague: state.leagueKey.selectedLeague,
      currentUser: state.auth.currentUser,
      username: state.auth.username
    };
  };

  /**
  Exporting the component for use in other components.
  */

export default connect(mapStateToProps, { deleteLeagueAction })(ReadyScreen);
