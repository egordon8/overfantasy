import React from 'react';
import firebase from 'firebase';
import * as firestore from 'firebase/firestore';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Text, FlatList, ScrollView, View } from 'react-native';
import Card from '../common/Card';
import CardSection from '../common/CardSection';
import Button from '../common/Button';
import LeagueDetail from '../LeagueDetail';
import LogoutButton from '../common/LogoutButton';
import Background from '../common/Background';
import Loading from './Loading';
import AltButton2 from '../common/AltButton2';
import {
  leagueSelectedFunction,
  setUserLeaguesAction
} from '../../redux/actions';

//routerkey: leagueFinder

/**
author: Elliot Gordon

This method corresponds to the main screen or main. It allows users to navigate
to league creation, league finder, player browsing, the schedule, their active
and archived leagues, and to log out.
*/

class Leagues extends React.Component {

  /**
  The constructor initialises the state object and retrieves props from
  parent components.
  */

  constructor(props) {
    super(props);

    this.ref = firebase.firestore()
    .collection('users')
    .doc(this.props.currentUser)
    .collection('theirLeagues');

    this.state = {
      loading: true,
      unsubscribe: null,
      leaguesArray: [],
      archives: false,
      switchString: 'Display archived leagues'
    };
  }

  /**
  This method establishes an ongoing connection with the database, watching the
  user's theirLeagues sub-collection.
  */

  componentDidMount() {
    this.unsubscribe = this.ref.onSnapshot(this.onUpdate);
  }

  /**
  Closes the database connection.
  */

  componentWillUnmount() {
    this.unsubscribe();
  }

  /**
  This method is called when there is a change in the user's list of active/archived
  leagues. It passes the list into state to be displayed.
  */

  onUpdate = (querySnapshot) => {
    const leaguesArray = [];
    querySnapshot.forEach((doc) => {
      const { name, type, leagueID } = doc.data();

      leaguesArray.push({
        key: doc.id,
        name: name,
        type: type,
        leagueID: doc.id,
        draftStatus: doc.data().draftStatus
      });
    });
    this.setState({
      leaguesArray,
      loading: false
    });
  }

  /**
  This method takes the contents of the leaguesArray array in state, and for
  each element, returns a LeagueDetail component.
  */

  renderLeagueArray() {
    return this.state.leaguesArray.map(leagueInfo =>
      <View key={leagueInfo.key}>
      <LeagueDetail leagueInfo={leagueInfo} archives={this.state.archives}/>
      </View>
    );
  }

  /**
  This conditionally renders space for styling purposes.
  */

  renderPadding() {
    if (this.state.leaguesArray.length<4 || this.state.archives) {
      return (
        <View style={{height:350, backgroundColor: '#fff'}} />
      );
    }
  }

  /**
  This method routes the user to the league finder component.
  */

  async goToLeagueFinder() {
    const arr = [];
    await this.state.leaguesArray.forEach(league => {
      arr.push(league.key);
    })
    await this.props.setUserLeaguesAction(arr);
    Actions.leagueFinder();
  }

  /**
  This method toggles between showing active and archived leagues.
  */

  switchArrayRendered() {
    if (!this.state.archives) {
      this.setState({
        archives: true,
        switchString: 'Display active leagues'
      });
    } else if (this.state.archives) {
      this.setState({
        archives: false,
        switchString: 'Display archived Leagues'
      });
    }
  }

  /**
  The render helper dictates what is shown on the screen, including showing the
  loading screen while data is being retrieved.
  */

  renderHelper() {
    if (this.state.loading) {
      return (
        <Loading message='Logging in...'/>
      );
    }
      return (
        <View style={{backgroundColor: '#fff'}}>
        <ScrollView >
<View style={{height:10, backgroundColor:'#fff', flex: 1}} />
        <CardSection style={{ backgroundColor:'#fff', paddingTop: 20, flexDirection: 'row', justifyContent: 'space-around', borderColor:'#fff'}} >
          <AltButton2 style={styles.buttonStyle} onPress={() => { Actions.createLeague(); }} > Create a league </AltButton2>

          <AltButton2 style={styles.buttonStyle} onPress={() => this.goToLeagueFinder()} > Join a league </AltButton2>
        </CardSection>

        <CardSection style={{ backgroundColor:'#fff', paddingTop: 20, flexDirection: 'row', justifyContent: 'space-around', borderColor:'#fff'}} >
          <AltButton2 style={styles.buttonStyle} onPress={() => Actions.schedule()} > Schedule </AltButton2>

          <AltButton2 style={styles.buttonStyle} onPress={() => { Actions.playerSearch(); }} > Browse players </AltButton2>
        </CardSection>

        <CardSection style={styles.csStyle2} >
          <Text style={styles.bigTextStyle2}>YOUR LEAGUES</Text>
        </CardSection>

                <CardSection style={styles.csStyle2} >
                  <View style={{flex:1, alignItems: 'center'}}>
                  <AltButton2  onPress={() => this.switchArrayRendered()} > {this.state.switchString} </AltButton2>
                  </View>
                </CardSection>

          <ScrollView contentContainerStyle={{backgroundColor:'#fff'}}>
            {this.renderLeagueArray()}
          </ScrollView>

          <CardSection style={styles.csStyle} >
            <LogoutButton />
          </CardSection>
        </ScrollView>
        {this.renderPadding()}
        </View>
      );
  }

  /**
  This screen dictates what the screen shows.
  */

  render() {
    return (
      <View>
        {this.renderHelper()}
      </View>
    );
  }
}

const styles = {
  bigTextStyle: {
    fontSize: 40,
    alignSelf: 'center',
    color: '#e18629',
    paddingLeft: 115,
    paddingTop: 5
  },
  bigTextStyle2: {
    fontSize: 40,
    alignSelf: 'center',
    color: '#e18629',
    paddingLeft: 31
  },
  buttonStyle: {
    width: 150,
    color: '#e18629'
  },
  csStyle:{
    paddingTop: 20,
    paddingBottom: 20,
    backgroundColor:'#fff',
    paddingLeft: 75,
    borderWidth: 0,
    borderColor: '#fff'
  },
  csStyle2:{
    backgroundColor:'#fff',
    paddingLeft: 5,
    borderWidth: 0,
    borderColor: '#fff'
  }
}

/**
The mapStateToProps object is used to attach variables from application state
(the Redux store) to this component as an object called props.
*/

const mapStateToProps = state => {
  return {
    currentUser: state.auth.currentUser,
    selectedLeague: state.leagueKey.selectedLeague,
    userLeagues: state.userKey.userLeagues,
    username: state.auth.username
  };
};

/**
Exporting the component for use in other components.
*/

export default connect(mapStateToProps, { leagueSelectedFunction, setUserLeaguesAction })(Leagues);
