import React from 'react';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import firebase from 'firebase';
import * as firestore from 'firebase/firestore';
import { Text, View, TimePickerAndroid, DatePickerAndroid } from 'react-native';
import Card from '../common/Card';
import CardSection from '../common/CardSection';
import Button from '../common/Button';

class DevInterface extends React.Component {
  state={
    players: null,
    textVal: "Here's some starter text",
    test: [],
    valval: 'right',
    timer: null,
    counter: 0,
    ended: false,
    time: null,
    date: null
   };


/*
  renderPlayers() {
    return this.state.players.map(player =>
      <Text>
        {player.name}
      </Text>
    );
  }
*/
//https://api.overwatchleague.com/players/3380?locale=en_US&expand=stats,stat.ranks
  componentWillMount() {
  //return fetch('https://api.overwatchleague.com/players/?locale=en_US&expand=stats')
return fetch('https://api.overwatchleague.com/players')
    .then((response) => response.json())
    .then((responseJson) => {
    //  console.log(responseJson);
      this.setState({ players: responseJson });
      //console.log(responseJson.content.length);
    })
    .catch((error) => {
      console.log('fetching OW stuff went wrong');
      console.error(error);
    });
}

  testDelete(){
    for (let i = 11; i < 59; i ++) {
      const x = i.toString();
      firebase.firestore().collection('leagues').doc(x).delete();
    }


  }

addCities() {
  this.ref = firebase.firestore().collection('leagues').doc('TEST_LEAGUE')
  .set({
    owners: {
      owner4: 'cantalope'
    }
  }, { merge: true })
  .then(console.log('successful write of marmot'));
}

  startTimerButton() {
    let timer = setInterval(this.tick, 1000);
    this.setState({ timer });
  }

  tick = () => {
    if (this.state.counter < 30) {
      this.setState({
        counter: this.state.counter + 1
      });
    }
    if (this.state.counter > 29) {
      this.setState({
        ended: true
      });
    }
  }

printy(){
  console.log('break');
  console.log('break');
  //sometimes this throws an error unreasonably
  const arraylol = [];
  for (let i = 0; i < 198; i++) {
    arraylol.push({
      id: this.state.players.content[i].id,
      name: this.state.players.content[i].name,
      role: this.state.players.content[0].attributes.role });
  }
  this.ref = firebase.firestore().collection('leagues').doc('TEST_LEAGUE')
  .set({
    playerArray: arraylol
  //});
  }, { merge: true });

  //ok this actually saves all the player ids
  //use this sort of format for adding players to db in league creation
//  for (let i = 0; i < this.state.players.content.length; i++) {
  //  arr.push(this.state.players.content[i].id);
//  }
//  console.log(arr);
  const arr = [];
/*  for (let i = 0; i < 198; i++) {
    const obj = { n: this.state.players[i].name, stats: this.state.players[i].stats.stats };
    arr.push(obj);
    }
    console.log(arr);*/
  //  const x = { name: this.state.players[0].name, stats: this.state.players[0].stats.stats };
    //console.log(x);
}

/*helper(){
  const arr = [];
  for (let i = 0; i < this.state.players.length; i++) {
      const obj = { n: this.state.players[i].name, stats: this.state.players[i].stats.stats };
      arr.push(obj);
      }
      return arr;
}*/

  printy2() {
    //const id = this.state.players[0].id;
    //const obj = { name: this.state.players[0].name, stats: this.state.players[0].stats.stats };
/*
    this.ref = firebase.firestore().collection('leagues').doc('TEST_LEAGUE')
    .set({
      players: {
        valval: ''
      }
    }, { merge: true });
*/
    //console.log(obj);

    const dd = new Date().getDate();
    console.log(dd);
  }

  playersToDB(){
/*
    const plpl = {
      name: this.state.players.content[0].name,
      id: this.state.players.content[0].id,
      headshot: this.state.players.content[0].headshot,
      team: this.state.players.content[0].teams[0].team.abbreviatedName,
      primaryColor: this.state.players.content[0].teams[0].team.primaryColor,
      secondaryColor: this.state.players.content[0].teams[0].team.secondaryColor
    };
*/
    const batch = firebase.firestore().batch();


    for (let i = 0; i < 198; i++) {
        const ID = this.state.players.content[i].id;

      this.refff = firebase.firestore().collection('players').doc(`${ID}`);
      batch.set(this.refff, {
/*
        name: this.state.players.content[i].name,
        id: ID,
        team: this.state.players.content[i].teams[0].team.abbreviatedName,
        primaryColor: this.state.players.content[i].teams[0].team.primaryColor,
        secondaryColor: this.state.players.content[i].teams[0].team.secondaryColor
*/
        role: this.state.players.content[i].attributes.role
}, { merge: true });
    }
    batch.commit();
  //  console.log(this.state.players.content);
}
/*
  conditional() {
    if (this.state.ended === false) {
      return (
        <Text>{this.state.counter}</Text>
      );
    } else {
      return (
        <Text>Timer finished</Text>
      );
    }
  }*/

  testButton() {
    //format works
    const dateString = "Wed Aug 07 2019 19:37:00 GMT+0100 (BST)";
    const d= new Date(dateString);
    console.log(d.getDay());

  }

  async tryx() {
    const pArray = [];
    for (let o = 0; o < this.state.players.content.length; o++) {
      pArray.push(this.state.players.content[o].id);
    }
    const batch = firebase.firestore().batch();
    this.ref = firebase.firestore().collection('players');
    /*await this.ref.get().then(x => {
      x.forEach(doc => {
        if (doc.data().id !== -1){
          pArray.push(doc.data().id);
        }
      });
    });*/
    //for (let i = 0; i < pArray.length; i++) {
      await fetch('https://api.overwatchleague.com/players/' +  `${pArray[50]}` + '?locale=en_US&expand=stats')
      .then((result) => result.json())
      .then((resultJSON) => {
        //console.log(resultJSON.data.stats.all);
        //console.log(resultJSON.data.player.id);
        const statObj = resultJSON.data.stats.all;
        const id = resultJSON.data.player.id;
        batch.set(this.ref.doc('4642'),
        { stats: statObj }, { merge: true})
        console.log(id)
        console.log(statObj)
      });

//    }
    batch.commit();
  }

  async datex() {
    const {action, year, month, day} = await DatePickerAndroid.open({
    // Use `new Date()` for current date.
    // May 25 2020. Month 0 is January.
    date: new Date(),
    minDate: new Date()
  });
  if (action !== DatePickerAndroid.dismissedAction) {
    //const y = new Date().getFullYear();
    const d = new Date(year, month, day, 0, 0, 0);
  //  d.setFullYear(2019);
    /*this.setState({
      date: d.toString()
    });*/
    this.timerSelect(d);
  }
  }

  async timerSelect(d) {
      const {action, hour, minute} = await TimePickerAndroid.open({
      hour: 14,
      minute: 0,
      is24Hour: true, // Will display '2 PM'
    });
    if (action !== TimePickerAndroid.dismissedAction) {
      this.setState({
        date: new Date(d.getFullYear(), d.getMonth(), d.getMonth(), hour, minute, 0, 0)
      })
      const f = new Date();
      console.log(this.state.date > f);
      console.log(this.state.date < f);
      console.log(this.state.date === f);
      console.log(this.state.date + ' alright then ' + f)


      //console.log(d.getYear() + '' + d.getMonth()  + '' + d.getDay()  + '' + hour  + '' + minute );
    }
  }



  render() {
    return (
      <Card
      style={{
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center' }}
      >

      <CardSection style={{ paddingTop: 50, paddingBottom: 50 }} >
        <Text> {this.state.textVal} </Text>
      </CardSection>
      <CardSection>
      <Button
      onPress={() => this.testDelete()}
      > this one </Button>
      </CardSection>

      <CardSection>
      </CardSection>

      </Card>
    );
  }
}


export default DevInterface;
