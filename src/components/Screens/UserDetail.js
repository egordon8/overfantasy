import React from 'react';
import { Text, View, Image, Linking } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import Card from '../common/Card';
import CardSection from '../common/CardSection';
import AltButton2 from '../common/AltButton2';
import Button from '../common/Button';
import { targetUserAction } from '../../redux/actions';

/**
author: Elliot Gordon

This component renders a stylised block displaying user information.
*/

const UserDetail = (props) => {
    const { username, score } = props.user;
    const { thumbnailStyle, thumbnailContainerStyle, imageStyle } = styles;

    const press = () => {
      props.targetUserAction(username);
      Actions.teamScreen();
      //console.log(username);
    }

    const getPlace = () => {
      if (props.index === 1) {
        return (
          <View style={{
            alignItems: 'center',
            height: 30,
            backgroundColor: '#fff',
            padding: 5,
            flexDirection: 'column',
            justifyContent: 'center'
          }}>
          <Text style={styles.textStyle}> In 2nd place </Text>

          </View>        );
      } else if (props.index === 2) {
        return (
          <View style={{
            alignItems: 'center',
            height: 30,
            backgroundColor: '#fff',
            padding: 5,
            flexDirection: 'row',
            justifyContent: 'center'
          }}>
          <Text style={styles.textStyle}> In 3rd place </Text>

          </View>
        );
      }
       else {
         return (
           <View style={{
             alignItems: 'center',
             height: 30,
             backgroundColor: '#fff',
             padding: 5,
             flexDirection: 'row',
             justifyContent: 'center'
           }}>
           <Text style={styles.textStyle}>In </Text>
           <Text style={styles.textStyle}>{props.index +1}th place</Text>
           <Text style={styles.textStyle}>place</Text>
           </View>
         );
       }
    }

    if (props.index===0 && props.bool) {
      return (
        <Card>
        <View style={{height:25}} />

          <View style={{
            alignItems: 'center',
            justifyContent: 'center',
            height: 30,
            backgroundColor: '#fff',
            padding: 5
          }}>
            <Text style={{
              fontSize: 20,
              fontWeight: '500',
              paddingTop: 10
            }}>The winner is</Text>
          </View>

          <View style={{
            alignItems: 'center',
            justifyContent: 'center',
            height: 30,
            backgroundColor: '#fff',
            padding: 5
          }}>
          <Text style={{
            fontSize: 30,
            color:'#e18629',
            fontWeight: '500',
            padding: 7
          }}>{username}</Text>
          </View>

          <View style={{
            alignItems: 'center',
            height: 30,
            backgroundColor: '#fff',
            padding: 5,
            flexDirection: 'row',
            justifyContent: 'center'
          }}>
          <Text style={styles.textStyle}>with </Text>
          <Text style={{
            color:'#e18629',
            fontSize:19,
            fontWeight:'500'
          }}>
            {score}
          </Text>
          <Text style={styles.textStyle}> points!</Text>
          </View>

          <View style={{
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor:'#fff'
          }}>
            <AltButton2 onPress={press.bind(this)}>
              View Team
            </AltButton2>
          </View>
          <View style={{height: 10, backgroundColor:'#fff'}} />
        </Card>
      );


    } else if (props.bool) {
      return (
        <Card>





            <View style={{
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: '#fff',
              padding: 5
            }}>

            <View style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 30,
              backgroundColor: '#fff',
              padding: 5,
              flexDirection: 'column'
            }}>
              {getPlace()}
            </View>

            <View style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 30,
              backgroundColor: '#fff',
              padding: 5
            }}>
            <Text style={{
              fontSize: 22,
              color:'#e18629',
              fontWeight: '500',
              padding: 7
            }}>{username}</Text>
            </View>


              <View style={{
                alignItems: 'center',
                height: 30,
                backgroundColor: '#fff',
                padding: 5,
                flexDirection: 'row',
                justifyContent: 'center'
              }}>
              <Text style={styles.textStyle}>with </Text>
              <Text style={{
                color:'#e18629',
                fontSize:19,
                fontWeight:'500'
              }}>
                {score}
              </Text>
              <Text style={styles.textStyle}> points!</Text>
              </View>

            </View>

            <View style={{
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor:'#fff'
            }}>
              <AltButton2 onPress={press.bind(this)}>
                View Team
              </AltButton2>
            </View>
            <View style={{height: 10, backgroundColor:'#fff'}} />

        </Card>

    )} else {
  return (

    <View key={username} style={{
      padding: 10,
      backgroundColor: '#fff'
    }}>
    <View

    style={{
      flexDirection: 'row',
      justifyContent: 'space-around',
      backgroundColor: '#e18629',
      height: 50,
      borderWidth: 2,
      borderColor: '#e18629',
      borderRadius: 5,
      alignItems: 'center'
    }}

    >
      <Text style={{
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold',
        flex:1,
        paddingLeft: 20
      }}>{username}</Text>

      <Text style={{
        textTransform: 'uppercase',
        color: '#fff',
        flex:1,
        fontWeight: 'bold',
        alignSelf: 'center'

      }}>SCORE: {score}</Text>

      <Button onPress={press.bind(this)}>
        VIEW
      </Button>
    </View>
    </View>
  );
}
};

const styles = {
  thumbnailStyle: {
    height: 50,
    width: 50
  },
  thumbnailContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10
  },
  imageStyle: {
    height: 300,
    flex: 1,
    width: null
  },
  textStyle: {
    fontSize: 19
  }
};

/**
The mapStateToProps object is used to attach variables from application state
(the Redux store) to this component as an object called props.
*/

  const mapStateToProps = state => {
    return {
      targetedUserID: state.userKey.targetedUserID
    }
  }

  /**
  Exporting the component for use in other components.
  */

export default connect(mapStateToProps, { targetUserAction })(UserDetail);
