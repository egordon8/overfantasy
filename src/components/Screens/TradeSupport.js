import React from 'react';
import { Actions } from 'react-native-router-flux';
import firebase from 'firebase';
import * as firestore from 'firebase/firestore';
import { connect } from 'react-redux';
import { Text, View, TouchableOpacity, ScrollView } from 'react-native';
import Card from '../common/Card';
import CardSection from '../common/CardSection';
import Loading from './Loading';
import Button from '../common/Button';
import Spinner from '../common/Spinner';
import AltButton from '../common/AltButton';
import AltButton2 from '../common/AltButton2';
import { targetUserAction,
  setTradeViewAction,
  setTradeStageAction,
  setOfferedPlayerAction,
  setRequestedPlayerAction,
  storeUserBackupAction, } from '../../redux/actions';

//routekey: tradeSupport

/**
author: Elliot Gordon

This is the second component used in the trade interface. It is used to allow
the user to select who he wants to trade with in trade proposal, and to confirm
their trade proposal.
*/

class TradeSupport extends React.Component {

  /**
  The constructor initialises the state object and retrieves props from
  parent components.
  */

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      users: [],
      spinner: false
    }

    this.ref = firebase.firestore()
    .collection('leagues')
    .doc(`${this.props.selectedLeague}`)
    .collection('owners');
  }

  /**
  This method retrieves a list of alll users in the league and adds them all to
  state, except for the currently logged-in user because they cannot trade with
  themselves.
  */

  async componentDidMount() {
    const arr = []
    await this.ref.get()
    .then(x => {
      x.forEach(doc => {
        const username = doc.data().username;
        if (!(username === this.props.username)) {
          arr.push(username);
        }
      });
    });
    await this.setState({
      loading: false,
      users: arr
    });
    console.log(arr);
  }

  /**
  This method is used to avoid a spinner (loading component) from being displayed
  at the wrong time. The spinner always needs to initialise as false.
  */

  componentWillUnmount(){
    this.setState({
      spinner: false
    })
  }

  /**
  This method is called when a user selects another user to trade with. It sends
  them to that user's trade screen and sets things up such that they can pick a
  player to request.
  */

  async press(user) {
    await this.props.targetUserAction(user);
    await this.props.setTradeViewAction(true);
    await this.props.setTradeStageAction(1);
    Actions.teamScreen();
  }

  /**
  This method renders the users in the league as a list, excluding the current user.
  */

  renderUsers() {
    return this.state.users.map(user =>
    <View key={user} style={{
      padding: 10,
      backgroundColor: '#fff'
    }}>
    <View

    style={{
      flexDirection: 'row',
      justifyContent: 'space-around',
      backgroundColor: '#e18629',
      height: 50,
      borderWidth: 2,
      borderColor: '#e18629',
      borderRadius: 5,
      alignItems: 'center'
    }}

    >
      <Text style={{
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold',
        flex:1,
        paddingLeft: 20
      }}>{user}</Text>

      <AltButton onPress={() => this.press(user)}>
        SELECT
      </AltButton>
    </View>
    </View>
  )
  }

  /**
  This method conditionally renders a loading symbol or a button depending on
  whether or not the user should be able to interact at this time.
  */

  buttonOrSpinner() {
    if (this.state.spinner) {
      return (
        <View style={{alignItems: 'center', justifyContent: 'center', backgroundColor:'#fff', height: 30}} >
          <Spinner />
        </View>
      );
    } else {
      return(
        <View>
        <View style={{
          alignItems: 'center',
          backgroundColor:'#fff',
          justifyContent: 'center'
        }}>
        <AltButton2 onPress={() => this.confirm()}> CONFIRM </AltButton2>
        </View>

        <View tag='spacer' style={{height:20, backgroundColor:'#fff'}} />

        <View style={{
          alignItems: 'center',
          backgroundColor:'#fff',
          justifyContent: 'center'
        }}>
        <AltButton2 onPress={() => this.cancel()}> CANCEL </AltButton2>
        </View>
        </View>
      );
    }
  }

  /**
  This method is called when the user cancels their trade proposal. It deletes the
  data that was stored during user and player selection.
  */

  async cancel(){
    await this.props.storeUserBackupAction(null);
    await this.props.setTradeViewAction(false);
    await this.props.setTradeStageAction(0);
    await this.props.targetUserAction('');
    Actions.popTo('tradeScreen', this.props);
  }

  /**
  This method takes the proposal information that has been collected during trade
  proposal and turns it into a proposal object and an offer object, and stores them
  in the database.
  */

  async confirm() {
    await this.setState({
      spinner: true
    });
  //add to proposals of this user
  const proposal = {
    targetUser: `${this.props.secondUser}`,
    requestedPlayer: this.props.requestedPlayer,
    offeredPlayer: this.props.offeredPlayer
  }

  const offer = {
    offeringUser: `${this.props.username}`,
    requestedPlayer: this.props.requestedPlayer,
    offeredPlayer: this.props.offeredPlayer
  }
  await firebase.firestore().collection('leagues')
  .doc(`${this.props.selectedLeague}`)
  .collection('trades')
  .doc(`${this.props.username}`)
  .update({
    proposals: firebase.firestore.FieldValue.arrayUnion(proposal)
  });

  await firebase.firestore().collection('leagues')
  .doc(`${this.props.selectedLeague}`)
  .collection('trades')
  .doc(`${this.props.secondUser}`)
  .update({
    offers: firebase.firestore.FieldValue.arrayUnion(offer)
  });


    this.cancel();
}

/**
The render helper dictates what is shown on the screen, including showing the
loading screen while data is being retrieved.
*/

  renderHelper() {
    if (this.state.loading) {
      return (
        <Loading message='Loading...' />
      );
    } else if (this.props.tradeStage === 3) {
      return (
        <Card>

        <View tag='spacer' style={{height:20, backgroundColor:'#fff'}} />

        <View style={{
          alignItems: 'center',
          backgroundColor:'#fff',
          justifyContent: 'center'
        }}>
          <Text style={{
          alignSelf: 'center',
          fontSize: 25,
        }}>Are you sure you want to trade</Text>
        </View>

        <View style={{
          alignItems: 'center',
          backgroundColor:'#fff',
          justifyContent: 'center'
        }}>
          <Text style={{
          alignSelf: 'center',
          fontSize: 30,
          color: '#e18629',
          fontWeight:'500'
        }}>{this.props.requestedPlayer.name}</Text>
        </View>

        <View style={{
          alignItems: 'center',
          backgroundColor:'#fff',
          justifyContent: 'center'
        }}>
          <Text style={{
          alignSelf: 'center',
          fontSize: 25,
        }}>for your</Text>
        </View>

        <View style={{
          alignItems: 'center',
          backgroundColor:'#fff',
          justifyContent: 'center'
        }}>
          <Text style={{
          alignSelf: 'center',
          fontSize: 30,
          color: '#e18629',
          fontWeight:'500'
        }}>{this.props.offeredPlayer.name}</Text>
        </View>

        <View style={{
          alignItems: 'center',
          backgroundColor:'#fff',
          justifyContent: 'center'
        }}>
          <Text style={{
          alignSelf: 'center',
          fontSize: 25,
        }}>?</Text>
        </View>

        {this.buttonOrSpinner()}
        <View tag='spacer' style={{height:400, backgroundColor:'#fff'}} />

        </Card>
      );
    } else {
      return (
        <Card
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
        backgroundColor:'#fff' }}
        >
        <ScrollView>
        <View style={{
          alignItems: 'center',
          paddingTop: 40,
          backgroundColor:'#fff',
          justifyContent: 'center'
        }}>
          <Text style={{
          alignSelf: 'center',
          fontSize: 25,
          color: '#e18629'
        }}>Select the user you</Text>
        </View>
        <View style={{
          alignItems: 'center',
          backgroundColor:'#fff',
          justifyContent: 'center'
        }}>
          <Text style={{
          alignSelf: 'center',
          fontSize: 25,
          color: '#e18629'
        }}>want to trade with</Text>
        </View>
        <View style={{height: 20, backgroundColor:'#fff'}} />
        <View style={{backgroundColor:'#fff'}}>
        {this.renderUsers()}
        </View>

        <View style={{height: 500, backgroundColor:'#fff'}} />

        </ScrollView>
        </Card>
      );
    }
  }

  /**
  This screen dictates what the screen shows.
  */

  render() {
    return (
      <View>
      {this.renderHelper()}
      </View>
    );
  }
}

/**
The mapStateToProps object is used to attach variables from application state
(the Redux store) to this component as an object called props.
*/

  const mapStateToProps = state => {
    return {
      selectedLeague: state.leagueKey.selectedLeague,
      playerID: state.playerKey.playerID,
      username: state.auth.username,
      tradeStage: state.userKey.tradeStage,
      requestedPlayer: state.userKey.requestedPlayer,
      offeredPlayer: state.userKey.offeredPlayer,
      targetedUserID: state.userKey.targetedUserID,
      secondUser: state.userKey.secondUser
    }
  }

  /**
  Exporting the component for use in other components.
  */

export default connect(mapStateToProps, {
  targetUserAction,
  setTradeViewAction,
  setTradeStageAction,
  setOfferedPlayerAction,
  setRequestedPlayerAction,
  storeUserBackupAction,

})(TradeSupport);
