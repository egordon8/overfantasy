import React from 'react';
import { Actions } from 'react-native-router-flux';
import firebase from 'firebase';
import * as firestore from 'firebase/firestore';
import { connect } from 'react-redux';
import { Text, View, ScrollView, Button } from 'react-native';
import Card from '../common/Card';
import CardSection from '../common/CardSection';
import AltButton from '../common/AltButton';
import LeagueDetail from '../LeagueDetail';
import Loading from './Loading';

/**
author: Elliot Gordon

This component is the league finder which allows users to view and join
leagues made by other users which they are not already a member of.
*/

class LeagueFinder extends React.Component {

  /**
  The constructor initialises the state object and retrieves props from
  parent components.
  */

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      leaguesArray: []
    };

    this.ref = firebase.firestore().collection('leagues');
  }

  /**
  This method establishes an ongoing connection with the database.
  */

  componentDidMount() {
    this.unsubscribe = this.ref.onSnapshot(this.onUpdate);
  }

  /**
  This method closes the connection to the database.
  */

  componentWillUnmount() {
    this.unsubscribe();
  }

  /**
  This method is called whenever a change in the watched section of the
  database occurs. It passes the contents of the leagues collection into the
  component to be displayed.
  */

  onUpdate = (querySnapshot) => {
    const arr = [];
    querySnapshot.forEach((doc) => {

      const item = {
        id: doc.data().leagueID,
        name: doc.data().name,
        type: doc.data().type,
        size: doc.data().size,
        key: doc.id,
        numberOfOwners: doc.data().numberOfOwners
      }
      console.log(doc.data().numberOfOwners);
      if ((item.id !== -1)
      && (item.type !== '')
      && (doc.data().draftStatus === 'notDone')
      ) {
        if (!this.props.userLeagues.includes(item.key)) {
      arr.push(item);
      console.log(doc.data().creator + ' ' + item.key + ' ' + this.props.userLeagues.includes(item.key));
      }
    }
    });
    console.log(this.props.userLeagues)

    this.setState({
      loading: false,
      leaguesArray: arr
    });
  }

  /**
  This method adds a user to a league, and adds the league to the user's list
  of active leagues.
  league: An object referencing the league being joined.
  */

  async join(league) {
    console.log(league);
    const batch = firebase.firestore().batch();
    const usersRef = firebase.firestore()
    .collection('users')
    .doc(`${this.props.currentUser}`)
    .collection('theirLeagues')
    .doc(`${league.id}`);

    batch.set(usersRef, {
      leagueID: league.id,
      name: league.name,
      type: league.type,
      draftStatus: 'active'
    });

    const leaguesRef = firebase.firestore()
        .collection('leagues')
        .doc(`${league.id}`);

    batch.update(leaguesRef,
      { owners: firebase.firestore.FieldValue.arrayUnion(`${this.props.currentUser}`) }
    );

    batch.update(leaguesRef,
      { ownerUsernames: firebase.firestore.FieldValue.arrayUnion(`${this.props.username}`) }
    );

    if ((league.size - 1) === league.numberOfOwners) {
      batch.update(leaguesRef,
        { draftStatus: 'ready' }
      );
    }

    batch.set(leaguesRef.collection('owners').doc(`${this.props.username}`),
      {
        score: 0,
        team: [null],
        teamSize: 0,
        username: `${this.props.username}`
      }
    )

    batch.set(leaguesRef.collection('trades').doc(`${this.props.username}`),
      {
        proposals: [null],
        offers: [null]
      }
    )

    await batch.commit();

    leaguesRef.update({
      numberOfOwners: firebase.firestore.FieldValue.increment(1)
    });

    Actions.pop();
  }

  /**
  Takes the leaguesArray and displays each league as a block with a join button.
  */

  renderLeagues() {
    return this.state.leaguesArray.map(league =>
      <CardSection key={league.key} style={{
        flexDirection: 'column',
        justifyContent: 'space-around',
        flex: 1,
        borderColor:'#fff',
        alignItems: 'center',
        backgroundColor: '#fff'
      }}>
      <View style={{
        width: 300, height: 150,
        flexDirection: 'column',
        alignItems: 'center',
        borderWidth:2,
        borderColor: '#e18629',
        borderRadius:5,
        shadowColor: 'blue',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 1.0,
    backgroundColor:'#e18629'

      }}>
      <View style={{
        alignItems: 'center',
      }}>
        <Text style={{
          paddingLeft: 10,
          paddingRight: 10,
          fontSize: 21,
          color: '#fff',
          fontWeight: 'bold'
          }}>{league.name}</Text>
      </View>
      <View style={{
        flex: 1,
        flexDirection: 'column'
      }} >
        <View style={{
          flex:1,
          flexDirection: 'column'
        }}>

          <View style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center'
          }}>
            <Text style={{ paddingLeft: 10, paddingRight: 10 }}>Draft Type:</Text>
            <Text style={{ fontSize: 15, paddingLeft: 10, paddingRight: 10 }}>{league.type}</Text>
          </View>

          <View style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center'
          }}>
            <Text style={{ paddingLeft: 10, paddingRight: 10 }}>Owners:</Text>
            <Text style={{ fontSize:15, paddingLeft: 10, paddingRight: 10 }}>{league.numberOfOwners + '/' + league.size}</Text>
          </View>

        </View>
        <View style={{
          flex: 1
        }}>
          <AltButton color='#e18629' title='Join' onPress={() => this.join(league)} style={{
            alignSelf: 'center',
            padding: 10,
            width: 150,
            height: 50
          }} >JOIN </AltButton>
          <View style={{height:10}} />
        </View>
        </View>
      </View>

      </CardSection>
    )
  }

  /**
  The render helper dictates what is shown on the screen, including showing the
  loading screen while data is being retrieved.
  */

  renderHelper() {
    if (this.state.loading) {
      return (
        <Loading message='Loading league finder...' />
      );
    } else {
      return (
        <ScrollView>
        <Card
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center' }}
        >
        <CardSection style={{borderColor:'#fff'}}>
          <View style={{
            alignItems: 'center',
            flex:1,
            height: 50
          }}>
            <Text style={{
              fontSize: 30,
              color: '#e18629',
              fontWeight: 'bold'
            }}>Join a League</Text>
          </View>
        </CardSection>

        {this.renderLeagues()}

        </Card>
        </ScrollView>
      );
    }
  }

  /**
  This screen dictates what the screen shows.
  */

  render() {
    return (
      <View>
      {this.renderHelper()}
      </View>
    );
  }
}

/**
The mapStateToProps object is used to attach variables from application state
(the Redux store) to this component as an object called props.
*/

  const mapStateToProps = state => {
    return {
      userLeagues: state.userKey.userLeagues,
      currentUser: state.auth.currentUser,
      selectedLeague: state.leagueKey.selectedLeague,
      username: state.auth.username
    };
  }

  /**
  Exporting the component for use in other components.
  */

export default connect(mapStateToProps, {})(LeagueFinder);
