import React from 'react';
import { Actions } from 'react-native-router-flux';
import firebase from 'firebase';
import * as firestore from 'firebase/firestore';
import { connect } from 'react-redux';
import { Text, View, ScrollView } from 'react-native';
import Card from '../common/Card';
import CardSection from '../common/CardSection';
import Button from '../common/Button';
import AltButton2 from '../common/AltButton2';
import Loading from './Loading';
import AltButton from '../common/AltButton';
import { targetUserAction } from '../../redux/actions';

/**
This component forms part of the trade interface. It allows users to propose
trades, view their proposals, and view offers that have been made to them.
*/

class TradeScreen extends React.Component {

  /**
  author: Elliot Gordon

  The constructor initialises the state object and retrieves props from
  parent components.
  */

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      offers: [],
      proposals: [],
      arr1: [],
      arr2: [],
      requestedPlayerToGive: null,
      offeredPlayerToGet: null,
      requestedPlayerToDelete: null,
      offeredPlayerToDelete: null
    }

    this.ref = firebase.firestore()
    .collection('leagues')
    .doc(`${this.props.selectedLeague}`)
    .collection('trades')
    .doc(`${this.props.username}`);
  }

  /**
  This method establishes an ongoing connection with the database.
  */

  componentDidMount() {
    this.unsubscribe = this.ref.onSnapshot(this.onUpdate);
  }

  /**
  This method closes the database connection.
  */

  componentWillUnmount() {
    this.unsubscribe();
  }

  /**
  This is called whenever a change in the watched area of the database occurs.
  Specifically, the user's trades sub-collection. This method passes the contents
  of that collection into state to be rendered.
  */

  onUpdate = (querySnapshot) => {
    const { offers, proposals } = querySnapshot.data();
    if (offers.length === 1) {
    } else {
      const arr = [];
      for (let i = 1; i < offers.length; i++){
        arr.push(offers[i]);
      }
      this.setState({ offers: arr })
    }

    if (proposals.length === 1) {
    } else {
      const arr = [];
      for (let i = 1; i < proposals.length; i++){
        arr.push(proposals[i]);
      }
      this.setState({ proposals: arr })
    }
    //console.log(this.state.proposals);
    this.setState({ loading: false })
  }

  /**
  This is called when an offer is declined. It deletes it from the database.
  offer: the offer object associated with the button pressed
  */

  async cancel(offer) {
    const refU3 = firebase.firestore()
    .collection('leagues')
    .doc(`${this.props.selectedLeague}`)
    .collection('trades')
    .doc(`${this.props.username}`);

    const refU4 = firebase.firestore()
    .collection('leagues')
    .doc(`${this.props.selectedLeague}`)
    .collection('trades')
    .doc(`${offer.offeringUser}`);

    const batch3 = firebase.firestore().batch();

    batch3.update(refU3,
      { offers: firebase.firestore.FieldValue.arrayRemove(offer) }
    );

    const proposalToDelete = {
      offeredPlayer: offer.offeredPlayer,
      requestedPlayer: offer.requestedPlayer,
      targetUser: `${this.props.username}`
    }

    batch3.update(refU4,
      { proposals: firebase.firestore.FieldValue.arrayRemove(proposalToDelete) }
    );

    await batch3.commit();
    await this.props.targetUserAction(`${this.props.username}`);
    Actions.replace('tradeScreen');
  }

  /**
  This method is called when a trade offer is accepted. It swaps the involved
  players, setting their starting values to false, and deletes the trade from
  the database.
  */

  async accept(offer) {
    const arrtest = [];
  this.reff = firebase.firestore()
  .collection('leagues')
  .doc(`${this.props.selectedLeague}`)
  .collection('owners')
  .doc(`${this.props.username}`);
  //get the team
  await this.reff.get()
  .then(doc => {
    const queriedTeam = doc.data().team;
    for (let i = 0; i < queriedTeam.length; i++) {
      if (queriedTeam[i] !== null) {
        arrtest.push(queriedTeam[i]);
      }
    }
    this.setState({
      arr1: arrtest
    });
    for (let i = 0; i < this.state.arr1.length; i++) {
      if (this.state.arr1[i] !== null) {
        if (this.state.arr1[i].id === offer.requestedPlayer.id) {
          const p = {
            id: this.state.arr1[i].id,
            name: this.state.arr1[i].name,
            role: this.state.arr1[i].role,
            stats: this.state.arr1[i].stats,
            team: this.state.arr1[i].team,
            starting: false
          };
          this.setState({
            requestedPlayerToGive: p,
            requestedPlayerToDelete: this.state.arr1[i]
          });
        }
      }
  }
  });

  const arrtest2 = [];
this.reff = firebase.firestore()
.collection('leagues')
.doc(`${this.props.selectedLeague}`)
.collection('owners')
.doc(`${offer.offeringUser}`);
//get the team
await this.reff.get()
.then(doc => {
  const queriedTeam = doc.data().team;
  for (let i = 0; i < queriedTeam.length; i++) {
    if (queriedTeam[i] !== null) {
      arrtest2.push(queriedTeam[i]);
    }
  }
  this.setState({
    arr1: arrtest2
  });
  for (let i = 0; i < this.state.arr1.length; i++) {
    if (this.state.arr1[i] !== null) {
      if (this.state.arr1[i].id === offer.offeredPlayer.id) {

        const p = {
          id: this.state.arr1[i].id,
          name: this.state.arr1[i].name,
          role: this.state.arr1[i].role,
          stats: this.state.arr1[i].stats,
          team: this.state.arr1[i].team,
          starting: false
        };

        this.setState({
          offeredPlayerToGet: p,
          offeredPlayerToDelete: this.state.arr1[i]
        });
      }
    }
}
});

console.log(this.state.offeredPlayerToGet);
console.log(this.state.offeredPlayerToDelete);
console.log(this.state.requestedPlayerToGive);
console.log(this.state.requestedPlayerToDelete);

  const batch = firebase.firestore().batch();

  const refU1 = firebase.firestore()
  .collection('leagues')
  .doc(`${this.props.selectedLeague}`)
  .collection('owners')
  .doc(`${this.props.username}`);

  const refU2 = firebase.firestore()
  .collection('leagues')
  .doc(`${this.props.selectedLeague}`)
  .collection('owners')
  .doc(`${offer.offeringUser}`);

  batch.update(refU1,
    { team: firebase.firestore.FieldValue.arrayRemove(this.state.requestedPlayerToDelete) }
  );
  batch.update(refU2,
    { team: firebase.firestore.FieldValue.arrayRemove(this.state.offeredPlayerToDelete) }
  );

  await batch.commit();

  const batch2 = firebase.firestore().batch();

  batch2.update(refU1,
    { team: firebase.firestore.FieldValue.arrayUnion(this.state.offeredPlayerToGet) }
  );
  batch2.update(refU2,
    { team: firebase.firestore.FieldValue.arrayUnion(this.state.requestedPlayerToGive) }
  );

  await batch2.commit();

  const refU3 = firebase.firestore()
  .collection('leagues')
  .doc(`${this.props.selectedLeague}`)
  .collection('trades')
  .doc(`${this.props.username}`);

  const refU4 = firebase.firestore()
  .collection('leagues')
  .doc(`${this.props.selectedLeague}`)
  .collection('trades')
  .doc(`${offer.offeringUser}`);

  const batch3 = firebase.firestore().batch();

  batch3.update(refU3,
    { offers: firebase.firestore.FieldValue.arrayRemove(offer) }
  );

  const proposalToDelete = {
    offeredPlayer: offer.offeredPlayer,
    requestedPlayer: offer.requestedPlayer,
    targetUser: `${this.props.username}`
  }

  batch3.update(refU4,
    { proposals: firebase.firestore.FieldValue.arrayRemove(proposalToDelete) }
  );

  await batch3.commit();
  await this.props.targetUserAction(`${this.props.username}`);
  Actions.replace('tradeScreen');
}

  /**
  This method generates a string key based on the trade object. This is required
  for objects to be rendered in an array. The keys must be unique, and these
  trade objects do not naturally have any unique qualities.
  */

  keyGen(obj) {
    const a =  obj.offeredPlayer.id + obj.requestedPlayer.id;
    const b = a.toString() + '' + this.props.username.toString();
    return b;
  }

  /**
  This method render's a user's offers.
  */

  renderOfferDetails() {
    return this.state.offers.map(offer =>
      <Card style={{ flex: 1, flexDirection: 'row' }} key={this.keyGen(offer)}>
      <View tag='holds the stats' style={{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        borderWidth: 3,
        borderColor: '#e18629',
        width: 340,
        borderRadius: 5,
        padding:  10
      }}>

      <View style={{
        borderColor:'#fff'
      }}>
        <AltButton2 style={{
          width: 80,
          backgroundColor:'red'
        }}
        onPress={() => this.cancel(offer)}
        disabled={false}>DECLINE</AltButton2>
      </View>

      <View tag='everything up to buttons' style={{
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        backgroundColor:'#fff'
      }}>

      <View style={styles.viewStyle}>
        <Text style={styles.smallTStyle}>
          Your
        </Text>
        <Text style={styles.tStyle}>
          {offer.requestedPlayer.name}
        </Text>
      </View>

      <View style={styles.viewStyle}>
        <Text style={styles.smallTStyle}>
          for
        </Text>
      </View>

      <View style={styles.viewStyle}>
        <Text style={styles.smallTStyle}>
          {offer.offeringUser}'s
        </Text>
        <Text style={styles.tStyle}>
          {offer.offeredPlayer.name}
        </Text>
      </View>

      </View>

        <View style={{
          borderColor:'#fff'
        }}>
          <AltButton2 style={{
            width: 80,
            backgroundColor:'green'
          }}
          onPress={() => this.accept(offer)}
          disabled={false}>ACCEPT</AltButton2>
        </View>
        </View>

        <View tag='spacer' style={{height: 15}} />
      </Card>
    )
  }

  /**
  This method renders a user's proposals as a list.
  */

  renderProposalDetails() {
    return this.state.proposals.map(proposal =>
      <Card style={{ flex: 1, flexDirection: 'row' }} key={this.keyGen(proposal)}>
      <View tag='holds the stats' style={{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        borderWidth: 3,
        borderColor: '#e18629',
        width: 340,
        borderRadius: 5,
        padding:  10
      }}>

      <View style={styles.viewStyle}>
        <Text style={styles.smallTStyle}>
          Your
        </Text>
        <Text style={styles.tStyle}>
          {proposal.offeredPlayer.name}
        </Text>
      </View>

      <View style={styles.viewStyle}>
        <Text style={styles.smallTStyle}>
          for
        </Text>
      </View>

      <View style={styles.viewStyle}>
        <Text style={styles.smallTStyle}>
          {proposal.targetUser}'s
        </Text>
        <Text style={styles.tStyle}>
          {proposal.requestedPlayer.name}
        </Text>
      </View>

        <View style={{
          borderColor:'#fff'
        }}>
          <AltButton style={{
            width: 100
          }}
            disabled={true}>PENDING</AltButton>
        </View>
        </View>

        <View tag='spacer' style={{height: 15}} />
      </Card>
    )
  }

  /**
  This method is used in displaying the user's offers if they have any.
  */

  renderOffers() {
    if (this.state.offers.length === 0) {
      return (
        <View style={{
          alignItems:'center',
          flexDirection:'row',
          height: 200, backgroundColor:'#fff'
        }}>
          <Text style={{
            fontSize: 20,
            paddingLeft: 90,
            paddingBottom: 50
          }}>
            You have no offers
          </Text>
          <View style={{height: 100, backgroundColor:'#fff'}} />

        </View>
      );
    } else {
      return (
        <View style={{backgroundColor:'#fff'}}>
        {this.renderOfferDetails()}
        </View>
      );
    }
  }

  /**
  This method is used in displaying a user's proposals if they have any.
  */

  renderProposals() {
    if (this.state.proposals.length === 0) {
      return (

        <View style={{
          alignItems:'center',
          flexDirection:'row',
          height: 200,
          backgroundColor:'#fff'
        }}>
          <Text style={{
            fontSize: 20,
            paddingLeft: 80,
            paddingBottom: 50
          }}>
            You have no proposals
          </Text>
        </View>
      );
    } else {
      return (
        <View style={{backgroundColor:'#fff'}}>
        {this.renderProposalDetails()}
        </View>
      );
    }
  }

  /**
  This method renders space conditionally for styling purposes.
  */

  renderPadding() {
    let sum = 0;
    for (let i = 0; i < this.state.proposals.length; i++) {
      sum +=1;
    }
    for (let j = 0; j < this.state.proposals.length; j++) {
      sum +=2;
    }
    if (sum < 6) {
      console.log(sum);
      return (
        <View style={{height: 400, backgroundColor:'#fff'}} />
      );
    }
  }

  /**
  The render helper dictates what is shown on the screen, including showing the
  loading screen while data is being retrieved.
  */

  renderHelper() {
    if (this.state.loading) {
      return (
        <Loading message='Loading trades...' />
      );
    } else {
      return (
        <ScrollView>

        <Card
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor:'#fff'
        }}
        >

        <View style={{
          paddingTop: 30,
          paddingBottom: 10,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#fff'
        }} >
          <AltButton2 onPress={() => Actions.tradeSupport()}> Propose a Trade </AltButton2>
        </View>

        <View style={{
          alignItems: 'center',
          paddingTop: 10,
          backgroundColor:'#fff',
        }}>
          <Text style={{
          alignSelf: 'center',
          fontSize: 25,
          color: '#e18629'
        }}>Your Proposals</Text>
         </View>

        <CardSection style={{ paddingTop: 20, paddingBottom: 20, backgroundColor:'#fff', borderColor:'#fff' }} >
        {this.renderProposals()}
        </CardSection>

        <View style={{
          alignItems: 'center',
          paddingTop: 10,
          backgroundColor:'#fff',
        }}>
          <Text style={{
          alignSelf: 'center',
          fontSize: 25,
          color: '#e18629'
        }}>Offers</Text>
         </View>

        <CardSection style={{ paddingTop: 20, paddingBottom: 20, backgroundColor:'#fff', borderColor: '#fff' }} >
        {this.renderOffers()}
        <View style={{height: 300, backgroundColor:'#fff'}} />
        </CardSection>

        </Card>
        </ScrollView>

      );
    }
  }

  /**
  This screen dictates what the screen shows.
  */

  render() {
    return (
      <View>
      {this.renderHelper()}
      </View>
    );
  }
}

const styles = {
  viewStyle: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  tStyle: {
    fontSize: 16,
    color: '#e18629',
    fontWeight: '600'
  },
  smallTStyle: {
    fontSize: 15,
    fontWeight:'500'
  }
};

/**
The mapStateToProps object is used to attach variables from application state
(the Redux store) to this component as an object called props.
*/

  const mapStateToProps = state => {
    return {
      username: state.auth.username,
      selectedLeague: state.leagueKey.selectedLeague
    }
  }

  /**
  Exporting the component for use in other components.
  */

export default connect(mapStateToProps, { targetUserAction })(TradeScreen);
