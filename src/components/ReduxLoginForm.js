import React, { Component } from 'react';
import firebase from 'firebase';
import * as firestore from 'firebase/firestore';
import { connect } from 'react-redux';
import { View, Text, ActivityIndicator } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Card from './common/Card';
import CardSection from './common/CardSection';
import AltInput from './common/AltInput';
import Background from './common/Background';
import Button from './common/Button';
import AltButton from './common/AltButton';
import Loading from './Screens/Loading';

import {
  emailChanged,
  passwordChanged,
  loginUser,
  createUser,
  usernameChanged,
  setErrorAction,
  flowSetAction } from '../redux/actions';
import Spinner from './common/Spinner';

/**
author: Elliot Gordon

This is the component that handles loggin the user in. It allows users to either
login or route to the create account screen.
*/

class ReduxLoginForm extends Component {

  /**
  The constructor initialises the state object and retrieves props from
  parent components.
  */

  constructor(props) {
    super(props);
    this.state = {
      confirmAccountCreate: null
    }
  }

  /**
  This method saves changes in the email text input field to application state.
  */

  onEmailChange(text) {
    this.props.emailChanged(text);
  }

  /**
  This method saves changes in the password text input field to application state.
  */

  onPasswordChange(text) {
    this.props.passwordChanged(text);
  }

  async onButtonPress() {
    const { email, password } = this.props;
    const ref = firebase.firestore().collection('users').doc(`${email}`);
    ref.get().then(doc => {
      this.props.usernameChanged(doc.data().username);
      console.log(this.props.username);
      this.props.flowSetAction('mainFlow');
      this.props.loginUser({ email, password });
    })
    .catch(error => {
      this.props.setErrorAction('Authentication Failed');
      return;
    })

  }

  /**
  This method is used to conditionally render error text.
  */

  renderError() {
    if (this.props.error) {
      return (
        <View style={{ backgroundColor: '#e18629', alignItems: 'center', justifyContent: 'center' }}>
          <Text style={styles.errorTextStyle}>
            {this.props.error}
          </Text>
        </View>
      );
    }
  }

  /**
  This method conditionally renders either a button, or a loading symbol to
  limit user action.
  */

  renderButton() {
    if (this.props.loading) {
      return (
        <View style={{
          backgroundColor: '#e18629',
          justifyContent: 'center',
          alignItems: 'center',
          paddingTop: 20,
          paddingRight: 160
        }}>
          <ActivityIndicator size='large' color='#fff' />
        </View>
    );
    }
    return (
      <Background >
      <View style={{alignItems: 'space-around', flex: 1,alignSelf: 'stretch'}}>
      <View style={{height:20}} />

        <CardSection style={styles.buttonStyle}>
          <AltButton
          disabled={
            this.props.email === ''
            || this.props.email === null
          }
          onPress={this.onButtonPress.bind(this)}>
            LOGIN
          </AltButton>
        </CardSection>
        <View style={{height:20}} />
        <CardSection style={styles.buttonStyle}>
            <AltButton onPress={() => {
              this.props.emailChanged('');
              this.props.passwordChanged('');
              this.props.usernameChanged('');
              Actions.createAccount();
            }} style={{alignSelf: 'center'}}>
              CREATE AN ACCOUNT
            </AltButton>
        </CardSection>
        </View>
      </Background>
    );
  }

  /**
  This screen dictates what the screen shows.
  */

  render() {
    return (
      <Background style={{ alignItems: 'space-around', flex: 1, flexDirection: 'column'}}>
      <View style={{height: 100, backgroundColor:'#e18629'}}/>
      <CardSection style={{backgroundColor:'#e18629', height: 120, borderColor: '#e18629', alignItems:'center'}}>
      <Text style={{ color: '#fff', fontSize: 45, paddingRight:18 }}>OVER.FANTASY</Text>
      </CardSection>

      <CardSection style={styles.cardSectionStyle}>
        <AltInput
          label='EMAIL'
          placeholder='example@email.com'
          onChangeText={this.onEmailChange.bind(this)}
          value={this.props.email}
        />
      </CardSection>

      <CardSection style={styles.cardSectionStyle}>
        <AltInput
          secureTextEntry
          label='PASSWORD'
          placeholder='example password'
          onChangeText={this.onPasswordChange.bind(this)}
          value={this.props.password}
        />
      </CardSection>

        {this.renderError()}


          {this.renderButton()}


      </Background>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: '#b00702',
    paddingRight: 80
  },
  cardSectionStyle: {
    backgroundColor: '#e18629',
    borderColor: '#e18629',
    position: 'relative',
    alignItems: 'center'
  },
  buttonStyle: {
    backgroundColor: '#e18629',
    borderColor: '#e18629',
    position: 'relative',
    alignItems: 'center',
    paddingRight: 75
  }
};

/**
The mapStateToProps object is used to attach variables from application state
(the Redux store) to this component as an object called props.
*/

const mapStateToProps = state => {
  return {
    email: state.auth.email,
    password: state.auth.password,
    error: state.auth.error,
    loading: state.auth.loading,
    username: state.auth.username
  };
};

/**
Exporting the component for use in other components.
*/

export default connect(mapStateToProps, {
  emailChanged,
  passwordChanged,
  loginUser,
  createUser,
  usernameChanged,
  setErrorAction,
  flowSetAction
 })(ReduxLoginForm);
