import React from 'react';
import firebase from 'firebase';
import * as firestore from 'firebase/firestore';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Text, FlatList, ScrollView, View } from 'react-native';
import Card from './common/Card';
import CardSection from './common/CardSection';
import Button from './common/Button';
import AltPlayerDetail from './AltPlayerDetail';
import Spinner from './common/Spinner';
import AltButton from './common/AltButton';
import DraftWait from './DraftWait';
import Loading from './Screens/Loading';
import {
  playerIDstoreAction
} from '../redux/actions';

/**
This component is used to control what the users sees at a given point during a
draft. It will either show a list of players to draft from, or the draft wait
screen.
*/

class DraftScreen extends React.Component {

  /**
  author: Elliot Gordon

  The constructor initialises the state object and retrieves props from
  parent components.
  */

  constructor(props) {
    super(props);
    this.ref = firebase.firestore()
    .collection('leagues')
    .doc(`${this.props.selectedLeague}`);

    this.size = 0;

    this.state = {
      loading: true,
      unsubscribe: null,
      pArray: [],
      drafting: false,
      currentDrafter: -1,
      playerCount: -1,
      counter: 0,
      timer: null,
      ended: false,
      lastDraftedP: '',
      lastDraftedU: '',
      theirTeam: null,
      type: null,
      backgroundCounter: 0,
      altTimer: null,
      draftingUser: null,
      forcingDraft: false,
      holdingUserData: null,
      isCreator: false,
      draftObj: null
    };
  }

  /**
  When this component mounts, it establishes two ongoing connections to the database
  (they are watching different values in different collections).
  */

  componentDidMount() {
    this.refx = firebase.firestore().collection('leagues').doc(`${this.props.selectedLeague}`).collection('owners')
    .doc(`${this.props.username}`);

    this.unsubscribe = this.ref.onSnapshot(this.onUpdate);


    this.unsub2 = this.refx.onSnapshot(this.onUpdate2);

  }

  /**
  This method closes the database connections. It also acts as a failsafe that
  automatically drafts a player for a user if the user navigates away from this
  screen while it is their turn to draft.
  */

  componentWillUnmount() {
    this.unsubscribe();
    this.unsub2();

    if(this.state.drafting) {
      this.autoPickFunction(false);
    }

    this.setState({
      timer: null,
      altTimer: null
    })
  }

  /**
  This method is called whenever the user's teamSize value increases.
  */

  onUpdate2 = (querySnapshot) => {
    const teamSize = querySnapshot.data().teamSize;
    const temp = [];
    this.setState({
      playerCount: teamSize
    });
  }

  /**
  This method is called whenever a value in the league's document changes. This
  method continuously passes the league's information into state and also is
  responsible for starting timers that maintain the flow of the draft.
  */

  onUpdate = (querySnapshot) => {
    clearInterval(this.state.timer);

    this.setState({
      timer: null,
      counter: 0
    })

      const playerArray = querySnapshot.data().playerArray;
      const val = querySnapshot.data().draftingNow;

      this.setState({
        //array of players being rendered
        pArray: playerArray,
        //is the component still loading info from db
        loading: false,
        //is it the current user's turn to draft or not (returns boolean)
        drafting: querySnapshot.data().owners[val] === this.props.currentUser,
        //which owner in the league is currently drafting
        currentDrafter: val,
        //size of the league
        size: querySnapshot.data().size,
        //league's draft type
        type: querySnapshot.data().type,
        //what was the last drafted player
        lastDraftedP: querySnapshot.data().lastDraftedPlayer,
        //which user drafted last
        lastDraftedU: querySnapshot.data().lastDraftedUser,
        //username of current drafter
        draftingUser: querySnapshot.data().ownerUsernames[val],
        //sets true if current user created draft
        isCreator: querySnapshot.data().creator === this.props.currentUser

      });
      if (this.state.type === 'Snake') {
        this.startTimer();
      }

      if (this.state.type === 'Autopick') {
        this.delayTimer();
      }
  }

  onCollectionUpdate = (querySnapshot) => {
    const pArray = [];
    querySnapshot.forEach((doc) => {
      const { playerArray } = doc.data();
      this.setState({
        pArray: playerArray
      });

    });
    this.setState({
      pArray,
      loading: false
    });

  }

  /**
  This method starts a timer.
  */

  startTimer() {
    const timer = setInterval(this.tick, 1000);
    this.setState({ counter: 0, timer });
  }

  /**
  This method increments the timer while it is less than 45.
  It also triggers the autopick for the user if they don't draft in 30 seconds,
  or if 45 seconds elapse and the user is the creator.
  */

  tick = () => {
    if (this.state.counter < 45) {
      this.setState({
        counter: this.state.counter + 1
      });
    }
    if (this.state.counter > 29 && this.state.counter < 31 && this.state.type === 'Snake' && this.state.drafting) {
      this.autoPickFunction(false);
      this.setState({
        counter: "zero"
      });
      clearInterval(this.state.timer);
      this.setState({
        timer: null
      });
    }
    if (this.state.counter > 44 && this.state.counter < 46 && this.state.type === 'Snake' && this.state.isCreator) {
      this.setState({
        counter: "zero"
      });
      clearInterval(this.state.timer);
      this.setState({
        timer: null
      });
      const x = true;
      this.autoPickFunction(x);

    }
  }

  /**
  This starts a timer.
  */

  delayTimer() {
    let timer = setInterval(this.tick2, 1000);
    this.setState({ counter: 0, timer });
  }

  /**
  This method increments the timer up to 15, 1 tick per second.
  It triggers autopick regularly in an autopick draft.
  */

  tick2 = () => {
    if (this.state.counter < 15 && this.state.type === 'Autopick') {
      this.setState({
        counter: this.state.counter + 1
      });
    }
    if (this.state.counter > 3 && this.state.counter < 5 && this.state.type === 'Autopick' && this.state.drafting && this.state.isCreator) {
      this.autoPickFunction(false);
      this.setState({
        counter: 'zero'
      });
      clearInterval(this.state.timer);
      this.setState({
        timer: null
      });
    }
    if (this.state.counter > 9 && this.state.counter < 11 && this.state.type === 'Autopick' && this.state.isCreator) {
      this.setState({
        counter: "zero"
      });
      clearInterval(this.state.timer);
      this.setState({
        timer: null
      });
      const x = true;
      this.autoPickFunction(x);

    }

  }

  /**
  This method randomly selects an available player and calls the draft function
  with this player as a parameter. It has two conditions that executed based on the
  value of bool. One condition drafts for the currently logged-in user, and the
  second drafts for the user whos turn it currently is to draft.
  */

  async autoPickFunction(bool) {
    console.log('bool log:')
    console.log(bool);
    const randomPlayerIndex
      = Math.floor(Math.random() * (this.state.pArray.length - 1)) + 1;
      if (bool === false){
        console.log('autopick thing false - bad')
    const obj = {
      id: this.state.pArray[randomPlayerIndex].id,
      name: this.state.pArray[randomPlayerIndex].name,
      role: this.state.pArray[randomPlayerIndex].role,
      stats: this.state.pArray[randomPlayerIndex].stats,
      team: this.state.pArray[randomPlayerIndex].team,
      username: this.props.username,
      currentDrafter: this.state.currentDrafter,
      size: this.state.size,
      playerCount: this.state.playerCount,
      selectedLeague: this.props.selectedLeague
      }
      this.setState({
        counter: 0
      })
      this.draftFunction(obj);


    }

    else {
      console.log('auto was true - good, whats next? should say gg probs')
      console.log(this.state.draftingUser);


        const obj = {
          id: this.state.pArray[randomPlayerIndex].id,
          name: this.state.pArray[randomPlayerIndex].name,
          role: this.state.pArray[randomPlayerIndex].role,
          stats: this.state.pArray[randomPlayerIndex].stats,
          team: this.state.pArray[randomPlayerIndex].team,
          username: this.state.draftingUser,
          currentDrafter: this.state.currentDrafter,
          size: this.state.size,
          playerCount: (this.state.playerCount - 1),
          selectedLeague: this.props.selectedLeague
          }
          console.log('draftObj');
          console.log(obj);
          this.setState({
            counter: 0
          })
          this.draftFunction(obj);

    }

  }

  /**
  This function drafts a player to a user's team, and removes the player from the
  list of available players.
  obj: the player object
  */

  async draftFunction(obj) {

    console.log('received object:')
    console.log(obj)
    const tl = 'TEST_LEAGUE';
      const reff = firebase.firestore()
      .collection('leagues')
      .doc(`${obj.selectedLeague}`);

    const player = {
      id: obj.id,
      name: obj.name,
      role: obj.role,
      stats: obj.stats,
      team: obj.team,
      starting: false
    };

    const playerForDelete = {
      id: obj.id,
      name: obj.name,
      role: obj.role,
      stats: obj.stats,
      team: obj.team
    };
    const ref2 = firebase.firestore().collection('leagues')
    .doc(`${obj.selectedLeague}`).collection('owners')
    .doc(obj.username);

    const batch = firebase.firestore().batch();

    const deleteRef = firebase.firestore().collection('leagues').doc(`${obj.selectedLeague}`);
    batch.update(deleteRef, {
      playerArray: firebase.firestore.FieldValue.arrayRemove(playerForDelete)
    })

    const cDrafter = obj.currentDrafter;
    const one = 1;
    const temp = cDrafter + one;

    if ((obj.playerCount === 9) && (temp.toString() === obj.size.toString())) {
      batch.update(reff, { draftStatus: 'complete' });
    }

    batch.update(ref2,
    { team: firebase.firestore.FieldValue.arrayUnion(player) });
    batch.update(ref2,
    { teamSize: firebase.firestore.FieldValue.increment(1) });

    if (temp.toString() === obj.size.toString()) {
      batch.update(reff,
        { draftingNow: 0 });
    } else {
      batch.update(reff,
        { draftingNow: firebase.firestore.FieldValue.increment(1) });
      }

      batch.update(deleteRef,
        { lastDraftedPlayer: obj.name }
      );

      batch.update(deleteRef,
        { lastDraftedUser: obj.username }
      );

      batch.commit()

  }

  /**
  This inverts the counter's value to look like a countdown.
  */

  getCounterTime() {
    const counter = this.state.counter;
    if (counter == 'zero') {
      return 0;
    } else {
      return (30-this.state.counter);
    }
  }

  /**
  This method either shows the amount of time the user has left to draft if there is any
  left, or it shows a loading symbol while the autopick is taking place.
  */

  secondsLeft() {
    if(true) {

      return (
        <View style={{
          backgroundColor: '#fff',
          alignItems: 'center',
          borderBottomWidth: 7,
          borderColor:'#e18629',
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center'
        }}>

          <Text style={styles.textStyle}>You have </Text>
          <Text style={styles.counterStyle}>{this.getCounterTime()}</Text>
          <Text style={styles.textStyle}> seconds to draft!</Text>
        </View>
      );
    } else {
      return (
        <View style={{
          backgroundColor: '#fff',
          alignItems: 'center',
          borderBottomWidth: 7,
          borderColor:'#e18629',
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center'
        }}>
          <Spinner />
        </View>
      );
    }
  }

  renderPs() {
    return this.state.pArray.map(album =>
      <View key={album.id} style={{backgroundColor:'#fff'}}>
        <AltPlayerDetail
        album={album}
        draft={this.draftFunction}
        currentDrafter={this.state.currentDrafter}
        size={this.state.size}
        playerCount={this.state.playerCount}
        selectedLeague={this.props.selectedLeague}
        />
      </View>
    );
  }

  /**
  The render helper dictates what is shown on the screen, including showing the
  loading screen while data is being retrieved.
  */

  renderHelper() {
    if (this.state.loading) {
      return (
        <Loading message='Loading draft' />
      );
    } else if ((!this.state.drafting) || (this.state.drafting && this.state.type === 'Autopick')) {
        return (
          <DraftWait data={this.state} />
        );
    } else if (this.state.drafting && this.state.type === 'Snake') {
      return (
          <Card
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center' }}
          >

          <View style={{
            width: 360,
            backgroundColor: '#fff',
            height: 60
          }}>
            {this.secondsLeft()}
          </View>


            <ScrollView>
              {this.renderPs()}
            </ScrollView>
          </Card>
        );
      }
    return (
      <Loading message='Loading draft' />
    );
  }

  /**
  This screen dictates what the screen shows.
  */

  render() {
    return (
      <View>
      {this.renderHelper()}
      </View>
    );
  }

}

const styles = {
  textStyle: {
    fontSize: 20,
    fontWeight: '500'
  },
  counterStyle: {
    fontSize: 22,
    fontWeight: '600',
    color: '#e18629'
  }
}

/**
The mapStateToProps object is used to attach variables from application state
(the Redux store) to this component as an object called props.
*/

const mapStateToProps = state => {
  return {
    currentUser: state.auth.currentUser,
    username: state.auth.username,
    selectedLeague: state.leagueKey.selectedLeague,
    leagueName: state.leagueKey.leagueName
  };
};

/**
Exporting the component for use in other components.
*/

export default connect(mapStateToProps, { playerIDstoreAction })(DraftScreen);
