import React from 'react';
import { View } from 'react-native';

/**
This component functions primarily as a background for elements in a list and is
based on work by Stephen Grider in his tutorial series The Complete React Native
+ Hooks Course [2019 Edition].
*/

const CardSection = (props) => {
  return (
    <View style={[styles.containerStyle, props.style]}>
      {props.children}
      </View>
  );
};

const styles = {
  containerStyle: {
    borderBottomWidth: 1,
    padding: 5,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    position: 'relative'
  }
};

/**
Exporting the component for use in other components.
*/

export default CardSection;
