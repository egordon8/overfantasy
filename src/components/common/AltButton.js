import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

/**
author: Elliot Gordon
This component is a button.
*/

const AltButton = (props) => {
  const { onPress, children } = props;
  const { buttonStyle, textStyle } = styles;

  if (!props.disabled){
  return (
    <TouchableOpacity
    disabled={props.disabled}
    style={[buttonStyle, props.style]}
    onPress={onPress} >
      <Text style={textStyle}>{children}</Text>
    </TouchableOpacity>
  );
} else {
  return (
    <TouchableOpacity
    disabled={props.disabled}
    style={[styles.disabledStyle, props.style]}
    onPress={onPress} >
      <Text style={styles.disabledTextStyle}>{children}</Text>
    </TouchableOpacity>
  );
}
};

const styles = {
  buttonStyle: {
    width: 200,
    borderRadius: 5,
    borderColor: '#e18629',
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 3
  },
  textStyle: {
    alignSelf: 'center',
    color: '#e18629',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10
  },
  disabledStyle: {
    width: 200,
    borderRadius: 5,
    borderColor: '#e18629',
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: '#b9b5b4',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 3
  },
  disabledTextStyle: {
    alignSelf: 'center',
    color: 'grey',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10
  },
};

/**
Exporting the component for use in other components.
*/

export default AltButton;
