import React from 'react';
import { TextInput, View, Text } from 'react-native';

/**
author: Elliot Gordon

This component is a text input box, used to allowed users to enter text. It
includes a text label describing what to enter.
*/

const Input = (props) => {
  const { label, value, onChangeText, placeholder, secureTextEntry } = props;
  const { inputStyle, containerStyle, labelStyle } = styles;

  return (
    <View style={containerStyle}>
    <Text style={labelStyle}>{ label }</Text>
      <TextInput
      secureTextEntry={secureTextEntry}
      autoCorrect={false}
      placeholder={placeholder}
      style={inputStyle}
      value={value}
      onChangeText={onChangeText}
      textAlign='center'
      />
    </View>
  );
};

const styles = {
  inputStyle: {
    color: '#fff',
    paddingRight: 5,
    fontSize: 18,
    flex: 1
  },
  labelStyle: {
    fontSize: 14,
    color: '#fff',
    paddingTop: 5
  },
  containerStyle: {
    height: 70,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor:'#e18629',
    borderColor:'#e18629',
    alignItems: 'center',
    alignSelf:'center',
    flex: 1
  }
};

/**
Exporting the component for use in other components.
*/

export default Input;
