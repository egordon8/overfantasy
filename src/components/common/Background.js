import React from 'react';
import { View } from 'react-native';

/**
author: Elliot Gordon

This component is used to create a consistent background for certain screens.
*/

const Background = (props) => {
  return (
    <View style= {[styles.containerStyle, props.style]}>
      {props.children}
    </View>
  );
};

const styles = {
  containerStyle: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '#e18629',
    alignItems: 'space-around'
  }
};

/**
Exporting the component for use in other components.
*/

export default Background;
