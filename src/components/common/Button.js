import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

/**
This component is a button and is based on work by Stephen Grider in his tutorial
series The Complete React Native + Hooks Course [2019 Edition].
*/

const Button = (props) => {
  const { onPress, children } = props;
  const { buttonStyle, textStyle } = styles;
  return (
    <TouchableOpacity

    style={[buttonStyle, props.style]}
    onPress={onPress} >
      <Text style={textStyle}>{children}</Text>
    </TouchableOpacity>
  );
};

const styles = {
  buttonStyle: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: 'white',
    borderRadius: 5,
    borderWidth: 0,
    borderColor: '#e18629',
    marginLeft: 5,
    marginRight: 5,
    elevation: 3
  },
  textStyle: {
    alignSelf: 'center',
    color: '#e18629',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10
  }
};

/**
Exporting the component for use in other components.
*/

export default Button;
