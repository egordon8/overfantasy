import React from 'react';
import firebase from 'firebase';
import {connect} from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Text, TouchableOpacity } from 'react-native';
import {
  viewOnlyAction,
  usernameChanged,
  setErrorAction,
  flowSetAction
} from '../../redux/actions';

/**
author: Elliot Gordon

This component returns a button which, when pressed, will log the user out
via the Firebase authentication service and set application state contents to
default.
*/


const LogoutButton = (props) => {
  const { buttonStyle, textStyle } = styles;

  const logout = () => {
    console.log(props.user);
    firebase.auth().signOut().then(function(){
      console.log('signed out')
    }).then(function() {
      console.log(props.user);

    })
    props.viewOnlyAction(false);
    props.usernameChanged('');
    props.setErrorAction('');
    props.flowSetAction('authFlow');
    Actions.reset('authFlow');

  }
  return (
    <TouchableOpacity
    onPress={() =>logout()}
    style={[buttonStyle, props.style]}>
      <Text style={textStyle}>Logout</Text>
    </TouchableOpacity>
  );
};

const styles = {
  buttonStyle: {
    width: 200,
    borderRadius: 5,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: '#e18629',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#ff0000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.9,
    elevation: 3
  },
  textStyle: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10
  }
};

const mapStateToProps = state => {
  return {
    user: state.auth.user
  };
}

/**
Exporting the component for use in other components.
*/

export default connect(mapStateToProps, { viewOnlyAction,
  usernameChanged,
  setErrorAction,
  viewOnlyAction,
  flowSetAction })(LogoutButton);
