import React from 'react';
import { View } from 'react-native';

/**
author: Elliot Gordon

This component functions as a background and is based on work by Stephen Grider
in his tutorial series The Complete React Native + Hooks Course [2019 Edition].
*/

const Card = (props) => {
  return (
    <View style= {styles.containerStyle}>
      {props.children}
    </View>
  );
};

const styles = {
  containerStyle: {

    borderColor: '#ddd',
    borderBottomWidth: 0,
    marginLeft:1,
    marginRight:1

  }
};

/**
Exporting the component for use in other components.
*/

export default Card;
