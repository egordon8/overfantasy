import React from 'react';
import { View, ActivityIndicator } from 'react-native';

/**
This component is an activity indicator and is based on work by Stephen Grider
in his tutorial series The Complete React Native + Hooks Course [2019 Edition].
*/

const Spinner = ({ size }) => {
  return (
    <View style={styles.spinnerStyle}>
    <ActivityIndicator size={size || 'large'} color='orange' />
    </View>
  );
};

const styles = {
  spinnerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
};

/**
Exporting the component for use in other components.
*/

export default Spinner;
