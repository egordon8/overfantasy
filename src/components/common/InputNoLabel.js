import React from 'react';
import { TextInput, View, Text, TouchableOpacity } from 'react-native';

/**
author: Elliot Gordon

This component is a text input box, used to allowed users to enter text.
*/

const InputNoLabel = (props) => {
  const { value, onChangeText, placeholder, secureTextEntry } = props;
  const { inputStyle, containerStyle, labelStyle } = styles;

  return (
    <View style={containerStyle}>
      <TextInput
      secureTextEntry={secureTextEntry}
      autoCorrect={false}
      placeholder={placeholder}
      style={inputStyle}
      value={value}
      onChangeText={onChangeText}
      textAlign='center'
      />
    </View>
  );
};

const styles = {
  inputStyle: {
    color: 'black',
    paddingRight: 5,
    fontSize: 18,
    flex: 1
  },
  labelStyle: {
    fontSize: 14,
    color: 'black',
    paddingTop: 5
  },
  containerStyle: {
    height: 70,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor:'#fff',
    borderColor:'#fff',
    alignItems: 'center',
    alignSelf:'center',
    flex: 1
  }
};

/**
Exporting the component for use in other components.
*/

export default InputNoLabel;
