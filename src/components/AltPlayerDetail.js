import React from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Text, View, Button, TouchableOpacity } from 'react-native';
import Card from './common/Card';
import CardSection from './common/CardSection';
import AltButton2 from './common/AltButton2'
import {
  playerIDstoreAction
} from '../redux/actions';

/**
author: Elliot Gordon

This component returns a stylised block representing a player, which also
displays some of their information.
*/

const AltPlayerDetail = (props) => {


    const { id, name, role, stats, team } = props.album;
    const { thumbnailStyle, thumbnailContainerStyle, headerContentStyle,
      headerTextStyle, imageStyle, viewStyle, tStyle } = styles;

      const onButtonPress = () => {
        //constructing player object
        const obj = {
          id,
          name,
          role,
          stats,
          team,
          username: props.username,
          currentDrafter: props.currentDrafter,
          size: props.size,
          playerCount: props.playerCount,
          selectedLeague: props.selectedLeague
        }

      //  props.playerIDstoreAction(ppp);
        //console.log(props);
        props.draft(obj);
      //  console.log('below is in deet, above is in draftF')
      //  console.log(props)
    };

    const supportOrOffense = () => {
      if (role === 'support') {
        return (
          <View style={viewStyle}>
            <Text style={tStyle}>
              HP/10
            </Text>
            <Text style={styles.smallTStyle}>
              {(stats.healing_avg_per_10m).toFixed(2)}
            </Text>
          </View>
        );
      } else {
        return (
          <View style={viewStyle}>
            <Text style={tStyle}>
              DMG/10
            </Text>
            <Text style={styles.smallTStyle}>
              {(stats.hero_damage_avg_per_10m).toFixed(2)}
            </Text>
          </View>
        );
      }
    }

    const onTouchable = () => {
      props.playerIDstoreAction(id);
      Actions.playerScreen2();
    };
// add stats here
  return (
    <View style={{
      backgroundColor:'#fff'
    }}>
    <TouchableOpacity onPress={onTouchable.bind(this)}>
      <CardSection style={{
        borderColor: '#e18629',
        borderWidth: 4,
        borderRadius: 5,
        elevation: 3,
        borderBottomWidth: 4,
        backgroundColor:'#fff'
      }}>

        <View style={headerContentStyle}>
          <View style={{ flexDirection: 'column', flex:7, alignItems: 'center' , justifyContent: 'space-around', backgroundColor:'#fff'}} tag='holds name and role'>
            <Text style={headerTextStyle}>
              {name}
            </Text>
            <Text style={styles.smallTStyle}>
              {role.toUpperCase()}
            </Text>
          </View>

          <View tag='holds the stats' style={{
            flexDirection: 'row', flex:10, alignItems: 'center', justifyContent: 'space-around'
          }}>
          <View style={viewStyle}>
            <Text style={tStyle}>
              K/10
            </Text>
            <Text style={styles.smallTStyle}>
              {(stats.eliminations_avg_per_10m).toFixed(2)}
            </Text>
          </View>

          {supportOrOffense()}

          <View style={viewStyle}>
            <Text style={tStyle}>
              U/10
            </Text>
            <Text style={styles.smallTStyle}>
              {(stats.ultimates_earned_avg_per_10m).toFixed(2)}
            </Text>
          </View>

          </View>

          <View tag='holds button' style={{flex:5, alignItems: 'center', justifyContent: 'center'}}>
          <AltButton2
          style={{
            width: 60
          }}
          onPress={onButtonPress.bind(this)}
          >DRAFT!</AltButton2>
          </View>

        </View>
      </CardSection>
      </TouchableOpacity>
      <View style={{height:10, backgroundColor: '#fff'}} />
    </View>
  );
};

const styles = {
  headerContentStyle: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    height: 50,
    width: 340,
    backgroundColor:'#fff'
  },
  headerTextStyle: {
    fontSize: 16,
    fontWeight:'600'
  },
  thumbnailStyle: {
    height: 50,
    width: 50
  },
  thumbnailContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10
  },
  imageStyle: {
    height: 300,
    flex: 1,
    width: null
  },
  viewStyle: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  tStyle: {
    fontSize: 15,
    color: '#e18629',
    fontWeight: '500'
  },
  smallTStyle: {
    fontSize: 15
  }
};

/**
The mapStateToProps object is used to attach variables from application state
(the Redux store) to this component as an object called props.
*/

const mapStateToProps = state => {
  return {
    currentUser: state.auth.currentUser,
    username: state.auth.username,
    playerID: state.playerKey.playerID
  };
};

/**
Exporting the component for use in other components.
*/

export default connect(mapStateToProps, { playerIDstoreAction })(AltPlayerDetail);
