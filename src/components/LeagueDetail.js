import React from 'react';
import { Text, View, Image, Linking , Button, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Card from './common/Card';
import CardSection from './common/CardSection';
import { leagueSelectedFunction, leagueNameSetter } from '../redux/actions';

/**
author: Elliot Gordon

This component is used in rendering stylised lists of leagues.
*/

const LeagueDetail = (props) => {

  const onButtonPress = () => {
    props.leagueSelectedFunction(`${props.leagueInfo.key}`);
    props.leagueNameSetter(`${props.leagueInfo.name}`);
    Actions.LDRFlow();

    console.log(props.archives)
    console.log(props.leagueInfo)
    //console.log(props.selectedLeague);
  }

//  render() {
  if ((props.archives && props.leagueInfo.draftStatus === 'archived') || (!props.archives && props.leagueInfo.draftStatus === 'active')) {
      return (
        <CardSection style={{ backgroundColor: '#fff',
        borderColor: '#fff'
      }}>
            <View style={{ flex: 1,
              height: 70,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-around',
              borderWidth: 2,
              borderColor: '#e18629',
              backgroundColor: '#fff',
              borderRadius:5
            }}>
                <View style={{flex:1, alignItems: 'center'}}>
                    <Text style={{ fontSize: 18 }} >{props.leagueInfo.name}</Text>
                </View>
                <View style={{flex:1, alignItems: 'center'}}>
                <Button
                onPress={onButtonPress.bind(this)}
                title='VIEW'
                style={{ paddingRight: 15, color: '#e18629',  }}
                color={'#e18629'}
                />
                </View>
            </View>
        </CardSection>
      )
    } else {
      return (
        <View />
      );
    }
//    }
};

/**
The mapStateToProps object is used to attach variables from application state
(the Redux store) to this component as an object called props.
*/

  const mapStateToProps = state => {
    return {
      selectedLeague: state.leagueKey.selectedLeague,
      selectedLeagueName: state.leagueKey.leagueName
    };
  }

  /**
  Exporting the component for use in other components.
  */

export default connect(mapStateToProps, { leagueSelectedFunction, leagueNameSetter })(LeagueDetail);
