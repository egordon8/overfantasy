import React from 'react';
import { Scene, Router } from 'react-native-router-flux';
import { Actions } from 'react-native-router-flux';
import ReduxLoginForm from './ReduxLoginForm';
import CreateLeague from './Screens/CreateLeague';
import DevInterface from './Screens/DevInterface';
import Leagues from './Screens/Leagues';
import DraftScreen from './DraftScreen';
import LeaguePage from './Screens/LeaguePage';
import PlayerSearchScreen from './Screens/PlayerSearchScreen';
import PlayerScreen from './Screens/PlayerScreen';
import LeagueDraftRouter from './Screens/LeagueDraftRouter';
import TeamScreen from './Screens/TeamScreen';
import TradeScreen from './Screens/TradeScreen';
import TradeSupport from './Screens/TradeSupport';
import LeagueFinder from './Screens/LeagueFinder';
import CreateAccount from './Screens/CreateAccount';
import Schedule from './Screens/Schedule';
import ArchivedLeague from './Screens/ArchivedLeague';
import ReadyScreen from './Screens/ReadyScreen';

/**
author: Elliot Gordon

This component is the way that the Router from react-native-router-flux is implemented.
It partitions the app into scenes and essentially stores the user's "journey" through
screens as a stack, so that the user can easily go back. Nesting scenes has been
used to limit user action, such as to avoid the user simply being able to press the back
button after loggin in, because to do that they should formally log out.
*/

const RouterComponent = () => {

  return (
    <Router navigationBarStyle={{ backgroundColor: '#e18629' }} navBarButtonColor='#fff'>

    <Scene key='root' hideNavBar>

      <Scene key='authFlow' initial>
        <Scene key='login' component={ReduxLoginForm} title='Login' initial hideNavBar/>
        <Scene key='dev' component={DevInterface} title='Hello, Dev' onExit={() => console.log('onexit executed')}/>
        <Scene key='createAccount' component={CreateAccount} title='Create an Account' hideNavBar/>


      </Scene>

      <Scene key='mainFlow' >
        <Scene key='leaguesScreen' component={Leagues} title='Main' initial/>
        <Scene key='createLeague' component={CreateLeague} title='Create League' />
        <Scene key='playerSearch' component={PlayerSearchScreen} title='Player Search' />
        <Scene key='playerScreen' component={PlayerScreen} title='' />
        <Scene key='schedule' component={Schedule} title='This Week' />
        <Scene key='leagueFinder' component={LeagueFinder} title='League Finder' />


      </Scene>

      <Scene key='LDRFlow' >
        <Scene key='LDRouter' component={LeagueDraftRouter} title='Leagues' initial/>
        <Scene key='teamScreen' component={TeamScreen} title='Team' />
        <Scene key='tradeScreen' component={TradeScreen} title='Trade' />
        <Scene key='tradeSupport' component={TradeSupport} title='Trade' />
        <Scene key='playerScreen2' component={PlayerScreen} title='' />
        <Scene key='archivedLeague' component={ArchivedLeague} title='League' />
        <Scene key='playerSearch2' component={PlayerSearchScreen} title='Player Search' />
        <Scene key='readyScreen' component={ReadyScreen} title='Search' />


      </Scene>

    </Scene>

    </Router>
  );
};

/**
Exporting the component for use in other components.
*/

export default RouterComponent;
