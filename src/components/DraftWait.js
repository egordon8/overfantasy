import React from 'react';
import firebase from 'firebase';
import * as firestore from 'firebase/firestore';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Text, View, TouchableOpacity, ScrollView } from 'react-native';
import Card from './common/Card';
import CardSection from './common/CardSection';
import Button from './common/Button';
import Spinner from './common/Spinner';
import Loading from './Screens/Loading';
import AltPlayerDetail from './AltPlayerDetail';
import AltButton2 from './common/AltButton2';
import { playerIDstoreAction } from '../redux/actions';

/**
This component, the waiting screen, is shown to users that are not currently drafting
in a snake draft, or always in an autopick draft. It lets them view the players they have drafted
and see what the last user to draft drafted.
*/

class DraftWait extends React.Component {

  /**
  author: Elliot Gordon

  The constructor initialises the state object and retrieves props from
  parent components.
  */

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      lastDraftedP: '',
      lastDraftedU: '',
      team: null
    }

  }

  /**
  When this component mounts, it establishes an ongoing connection to the database
  with this method.
  */

  componentDidMount() {
    this.refDW = firebase.firestore().collection('leagues').doc(`${this.props.selectedLeague}`).collection('owners')
    .doc(`${this.props.username}`);
    this.unsubscribeDW = this.refDW.onSnapshot(this.onUpdateDW);
  }

  /**
  This method closes the database connection.
  */

  componentWillUnmount() {
    this.unsubscribeDW();
  }

  /**
  This continously stores and updates data in state based on the contents of the database.
  Specifically, it shows the players in the user's team.
  */

  onUpdateDW = (querySnapshotDW) => {
    const team = querySnapshotDW.data().team;
    const arr = [];
    for (let i = 0; i < team.length; i++) {
      if (team[i] !== null){
        arr.push(team[i]);
      }
    }
    this.setState({
      team: arr,
      loading: false
    });
  }

  /**
  This method is called as an onPress function when a button is pressed. It is
  used to direct the user to the player's information screen.
  */

  onTouchable() {
    this.props.playerIDstoreAction(id);
    Actions.playerScreen2();
  };

  /**
  This method returns a player's healing or damaging information depending on
  whether they are of the support role or offense/tank respectively.
  player: the player who is being displayed, player object
  */

  supportOrOffense(player) {
    if (player.role === 'support') {
      return (
        <View style={styles.viewStyle}>
          <Text style={styles.tStyle}>
            HP/10
          </Text>
          <Text style={styles.smallTStyle}>
            {(player.stats.healing_avg_per_10m).toFixed(2)}
          </Text>
        </View>
      );
    } else {
      return (
        <View style={styles.viewStyle}>
          <Text style={styles.tStyle}>
            DMG/10
          </Text>
          <Text style={styles.smallTStyle}>
            {(player.stats.hero_damage_avg_per_10m).toFixed(2)}
          </Text>
        </View>
      );
    }
  }

  /**
  This function takes the players in the user's team and renders each as a
  stylised block displaying their name and some statistics.
  */

  renderPlayers() {
    return this.state.team.map(player =>
      <View style={{
        backgroundColor:'#fff'
      }}>
      <TouchableOpacity >
        <CardSection style={{
          borderColor: '#e18629',
          borderWidth: 4,
          borderRadius: 5,
          elevation: 3,
          borderBottomWidth: 4,
          backgroundColor:'#fff'
        }}>

          <View style={styles.headerContentStyle}>
            <View style={{ flexDirection: 'column', flex:5, alignItems: 'center' , justifyContent: 'space-around', backgroundColor:'#fff'}} tag='holds name and role'>
              <Text style={styles.headerTextStyle}>
                {player.name}
              </Text>
              <Text style={styles.smallTStyle}>
                {player.role.toUpperCase()}
              </Text>
            </View>

            <View tag='holds the stats' style={{
              flexDirection: 'row', flex:10, alignItems: 'center', justifyContent: 'space-around'
            }}>
            <View style={styles.viewStyle}>
              <Text style={styles.tStyle}>
                K/10
              </Text>
              <Text style={styles.smallTStyle}>
                {(player.stats.eliminations_avg_per_10m).toFixed(2)}
              </Text>
            </View>

            {this.supportOrOffense(player)}

            <View style={styles.viewStyle}>
              <Text style={styles.tStyle}>
                U/10
              </Text>
              <Text style={styles.smallTStyle}>
                {(player.stats.ultimates_earned_avg_per_10m).toFixed(2)}
              </Text>
            </View>

            </View>


          </View>
        </CardSection>
        </TouchableOpacity>
        <View style={{height:10, backgroundColor: '#fff'}} />
      </View>
    )

  }

  /**
  This method displays the last player drafted by the last user.
  */

  renderLastDraft() {
    if (this.props.data.lastDraftedU === '') {
      return (
        <View />
      );
    } else {
      return (
        <View style={{
          backgroundColor: '#fff',
          flexDirection: 'row',
          alignSelf: 'center',
          justifyContent: 'center',
          alignSelf: 'stretch',
          paddingTop: 20
        }}>
        <Text style={styles.ldNameStyle}>{this.props.data.lastDraftedU}</Text>
        <Text style={{
          fontSize: 18,
          fontWeight: '400'
        }}> just drafted </Text>
        <Text style={styles.ldNameStyle}>{this.props.data.lastDraftedP}</Text>
        <Text style={{
          fontSize: 18,
          fontWeight: '400'
        }}>!</Text>
        </View>
      );
    }
  }

  /**
  This method conditionally renders space for styling purposes.
  */

  renderPadding() {
    if (this.state.team.length < 7) {
      return (
        <View style={{
          height: 500,
          backgroundColor: '#fff'
        }} />
      );
    }
  }

  /**
  The render helper dictates what is shown on the screen, including showing the
  loading screen while data is being retrieved.
  */

  renderHelper() {
    if (this.state.loading) {
      return (
        <Loading message='Loading...' />
      );
    } else if (this.state.team.length === 0) {
      return (
        <Card
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#fff',
          height: 800
        }}
        >
        <View style={{height: 200, backgroundColor:'#fff'}} />

        <View style={{
          alignItems: 'center',
          justifyContent: 'center',
          height: 30,
          backgroundColor: '#fff',
          padding: 5,
          paddingTop: 20
        }}>
          <Text style={{
            fontSize: 18,
            fontWeight: '400',
            color: '#e18629'
          }}> Please wait </Text>
        </View>

        {this.renderLastDraft()}

        <View style={{
          alignItems: 'center',
          justifyContent: 'center',
          height: 50,
          backgroundColor: '#fff',
          padding: 5
        }}>
        <Text style={{
          fontSize: 30,
          color:'#e18629',
          fontWeight: '500',
          paddingTop: 10
        }}>Your team is empty</Text>
        </View>
        <View style={{height: 400, backgroundColor:'#fff'}} />
        </Card>
      );
    } else {
      return(
        <Card
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#fff',
          height: 800
        }}
        >
        <ScrollView>
        <View style={{
          alignItems: 'center',
          justifyContent: 'center',
          height: 30,
          backgroundColor: '#fff',
          padding: 5,
          paddingTop: 20
        }}>
          <Text style={{
            fontSize: 18,
            fontWeight: '400',
            color: '#e18629'
          }}> Please wait </Text>
        </View>

        {this.renderLastDraft()}

        <View style={{
          alignItems: 'center',
          justifyContent: 'center',
          height: 50,
          backgroundColor: '#fff',
          padding: 5
        }}>
        <Text style={{
          fontSize: 30,
          color:'#e18629',
          fontWeight: '500'
        }}>Your Team</Text>
        </View>


        <View style={{
          backgroundColor: '#fff'
        }}>
        {this.renderPlayers()}
        </View>

        {this.renderPadding()}

        </ScrollView>

        </Card>
      );
    }
  }

  /**
  This screen dictates what the screen shows.
  */

  render() {
    return (
      <View>
      {this.renderHelper()}
      </View>
    );
  }
}

const styles = {
  headerContentStyle: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    height: 50,
    width: 340,
    backgroundColor:'#fff'
  },
  headerTextStyle: {
    fontSize: 16,
    fontWeight:'600'
  },
  viewStyle: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  tStyle: {
    fontSize: 15,
    color: '#e18629',
    fontWeight: '500'
  },
  smallTStyle: {
    fontSize: 15
  },
  ldNameStyle: {
    fontSize: 18,
    color:'#e18629',
    fontWeight: '600'
  }
};

/**
The mapStateToProps object is used to attach variables from application state
(the Redux store) to this component as an object called props.
*/

  const mapStateToProps = state => {
    return {
      selectedLeague: state.leagueKey.selectedLeague,
      username: state.auth.username
    };
  }

  /**
  Exporting the component for use in other components.
  */

export default connect(mapStateToProps, { playerIDstoreAction })(DraftWait);
