import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER,
  CREATE_ACCOUNT,
  STORE_USER,
  USERNAME_CHANGED,
  ERROR_SET
} from '../actions/types';

/**

author: Elliot Gordon

These are the default values of all variables in this reducer.
This file is based on work by Stephen Grider in his tutorial series The Complete
React Native + Hooks Course [2019 Edition].
*/

const INITIAL_STATE = {
  email: '',
  password: '',
  user: null,
  error: '',
  loading: false,
  currentUser: null,
  username: '' };

  /**
  This switch statement allows actions (the functions found in the actions files)
  to act on the reducer.
  */

export default(state = INITIAL_STATE, action) => {
  switch (action.type) {
    case EMAIL_CHANGED:
      return { ...state, email: action.payload, error: '' };
    case PASSWORD_CHANGED:
        return { ...state, password: action.payload, error: '' };
    case LOGIN_USER:
        return { ...state, loading: true, error: '' };
    case LOGIN_USER_SUCCESS:
        return { ...state,
          email: '',
          password: '',
          error: '',
          loading: false,
          user: action.payload
        };
    case LOGIN_USER_FAIL:
      return { ...state, error: 'Authentication Failed.', loading: false };
    case CREATE_ACCOUNT:
      return { ...state, loading: true, error: '' };
    case STORE_USER:
      return { ...state, currentUser: action.payload };
    case USERNAME_CHANGED:
      return { ...state, username: action.payload, error: '' };
    case ERROR_SET:
      return { ...state, error: action.payload };
    default:
      return state;
  }
};
