import {
  PLAYER_ID_STORE
} from '../actions/types';

//author: Elliot Gordon


/**
This is the default value for playerID.
*/

const INITIAL_STATE = {
  playerID: ''
};

/**
This switch statement allows actions (the functions found in the actions files)
to act on the reducer.
*/

export default(state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PLAYER_ID_STORE:
      return { ...state, playerID: action.payload };
    default:
      return state;
  }
};
