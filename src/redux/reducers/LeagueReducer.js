import {
  LEAGUE_SELECTED,
  LEAGUE_NAME_SET,
  DELETING_LEAGUE,
  VIEW_ONLY
} from '../actions/types';

//author: Elliot Gordon


/**
These are the default values of all variables in this reducer.
*/

const INITIAL_STATE = {
  selectedLeague: 'NULL_LEAGUE',
  leagueName: 'NULL_TITLE',
  deletingLeague: false,
  viewOnly: false
};

/**
This switch statement allows actions (the functions found in the actions files)
to act on the reducer.
*/

export default(state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LEAGUE_SELECTED:
      return { ...state, selectedLeague: action.payload };
    case LEAGUE_NAME_SET:
      return { ...state, leagueName: action.payload };
    case DELETING_LEAGUE:
      return { ...state, deletingLeague: action.payload };
    case VIEW_ONLY:
      return { ...state, viewOnly: action.payload };
    default:
      return state;
  }
};
