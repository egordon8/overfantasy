import { combineReducers } from 'redux';
import AuthenticationReducer from './AuthenticationReducer';
import PlayerReducers from './PlayerReducers';
import LeagueReducer from './LeagueReducer';
import UserReducer from './UserReducer';

/**
author: Elliot Gordon

Each of the below reducers is a section of the application state store. This
file gives each of them a keyword for easy access and exports them for use in
other components.
*/

export default combineReducers({
  auth: AuthenticationReducer,
  playerKey: PlayerReducers,
  leagueKey: LeagueReducer,
  userKey: UserReducer
});
