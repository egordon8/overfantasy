import {
  TARGETED_USER,
  SET_TRADE_VIEW,
  SET_TRADE_STAGE,
  SET_REQUESTED_PLAYER,
  SET_OFFERED_PLAYER,
  STORE_USER_BACKUP,
  SET_USER_LEAGUES,
  TIMER_STARTED,
  FLOW_SET
} from '../actions/types';

//author: Elliot Gordon


/**
These are the default values of all variables in this reducer.
*/

const INITIAL_STATE = {
  targetedUserID: '',
  tradeView: false,
  tradeStage: 0,
  requestedPlayer: null,
  offeredPlayer: null,
  secondUser: null,
  userLeagues: null,
  timerStarted: false,
  routerFlow: 'authFlow'
};

/**
This switch statement allows actions (the functions found in the actions files)
to act on the reducer.
*/

export default(state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TARGETED_USER:
      return { ...state, targetedUserID: action.payload };
    case SET_TRADE_VIEW:
      return { ...state, tradeView: action.payload };
    case SET_TRADE_STAGE:
      return { ...state, tradeStage: action.payload };
    case SET_OFFERED_PLAYER:
      return { ...state, offeredPlayer: action.payload };
    case SET_REQUESTED_PLAYER:
      return { ...state, requestedPlayer: action.payload };
    case STORE_USER_BACKUP:
      return { ...state, secondUser: action.payload };
    case SET_USER_LEAGUES:
      return { ...state, userLeagues: action.payload };
    case TIMER_STARTED:
      return { ...state, timerStarted: action.payload };
    case FLOW_SET:
      return { ...state, routerFlow: action.payload };
    default:
      return state;
  }
};
