import firebase from 'firebase';
import * as firestore from 'firebase/firestore';
import { Actions } from 'react-native-router-flux';
import {
  LEAGUE_SELECTED,
  LEAGUE_NAME_SET,
  DELETING_LEAGUE,
  VIEW_ONLY
} from './types';

//author: Elliot Gordon


/**
This action stores the selected league ID to application state.
*/

export const leagueSelectedFunction = (text) => {
  return {
    type: LEAGUE_SELECTED,
    payload: text
  };
};

/**
This action stores the specified league name to application state.
*/

export const leagueNameSetter = (text) => {
  return {
    type: LEAGUE_NAME_SET,
    payload: text
  };
};

/**
This action stores the fact that a league is currently being deleted to application state.
*/

export const deleteLeagueAction = (boolean) => {
  return {
    type: DELETING_LEAGUE,
    payload: boolean
  };
};

/**
This action stores the fact that any teams being viewed should be non-interactive at
the moment to application state.
*/

export const viewOnlyAction = (boolean) => {
  return {
    type: VIEW_ONLY,
    payload: boolean
  };
};
