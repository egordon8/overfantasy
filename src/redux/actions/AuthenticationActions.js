import firebase from 'firebase';
import * as firestore from 'firebase/firestore';
import { Actions } from 'react-native-router-flux';
import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER,
  CREATE_ACCOUNT,
  STORE_USER,
  USERNAME_CHANGED,
  ERROR_SET
 } from './types';

// author: Elliot Gordon


 /**
 A Redux action that updates the email value in application state.
 */

export const emailChanged = (text) => {
  return {
    type: EMAIL_CHANGED,
    payload: text
  };
};

/**
A Redux action that updates the username value in application state.
*/

export const usernameChanged = (text) => {
  return {
    type: USERNAME_CHANGED,
    payload: text
  };
};

/**
A Redux action that updates the password value in application state.
*/

export const passwordChanged = (text) => {
  return {
    type: PASSWORD_CHANGED,
    payload: text
  };
};

/**
A Redux action that updates the error text value in application state.
*/

export const setErrorAction = (text) => {
  return {
    type: ERROR_SET,
    payload: text
  };
};

/**
This action attempt to log the user in.
Based on work by Stephen Grider in his tutorial series The Complete React Native
+ Hooks Course [2019 Edition].
*/

export const loginUser = ({ email, password }) => {
  return (dispatch) => {
    dispatch({ type: LOGIN_USER });
  firebase.auth().signInWithEmailAndPassword(email, password)
  .then(user =>
    loginUserSuccess(dispatch, user))
    .catch(() => {
      loginUserFail(dispatch);
      console.log('Email/password combo not recognised.');
    });
  };
};

/**
This Redux action attempts to create an account and initialise it in the database.
Based on work by Stephen Grider in his tutorial series The Complete React Native
+ Hooks Course [2019 Edition].
*/

export const createUser = ({ email, password, username }) => {
  return (dispatch) => {
    dispatch({ type: CREATE_ACCOUNT });
    firebase.auth().createUserWithEmailAndPassword(email, password)
    .then(firebase.firestore().collection('users').doc(email).set({
      username: username,
      email: email
    })
  )
    .then(user => loginUserSuccess(dispatch, user))
    .catch(() => {
      loginUserFail(dispatch);
      console.log('Error in AuthenticationActions createUser.');
    });
  };
};

/**
This action is executed when the user successfully logs in. It stores the
user object to application state and routes the user to the main screen.
Based on work by Stephen Grider in his tutorial series The Complete React Native
+ Hooks Course [2019 Edition].
*/

const loginUserSuccess = (dispatch, user) => {
  dispatch({
    type: LOGIN_USER_SUCCESS,
    payload: user
  });
  //console.log(user.user.email);
  storeUser(dispatch, user);
  Actions.mainFlow();
};

/**
This action is executed when the user fails to log in. It sets error text to display.
Based on work by Stephen Grider in his tutorial series The Complete React Native
+ Hooks Course [2019 Edition].
*/

const loginUserFail = (dispatch) => {
  dispatch({ type: LOGIN_USER_FAIL });
};

/**
This action stores the user object retrieved from the firebase authentication service
after the user succesfully logs in.
*/

const storeUser = (dispatch, user) => {
  const userEmailForPayload = user.user.email;
  dispatch({
    type: STORE_USER,
    payload: userEmailForPayload
  });
};
