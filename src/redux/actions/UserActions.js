import {
  TARGETED_USER,
  SET_TRADE_VIEW,
  SET_TRADE_STAGE,
  SET_OFFERED_PLAYER,
  SET_REQUESTED_PLAYER,
  STORE_USER_BACKUP,
  SET_USER_LEAGUES,
  TIMER_STARTED,
  FLOW_SET
} from './types';

//author: Elliot Gordon


/**
This action stores the selected username to application state.
*/

export const targetUserAction = (text) => {
  return {
    type: TARGETED_USER,
    payload: text
  };
};

/**
This action sets the trade view boolean to be the supplied value.
*/

export const setTradeViewAction = (boolean) => {
  return {
    type: SET_TRADE_VIEW,
    payload: boolean
  };
};

/**
This action sets the trade stage to be the supplied value.
*/

export const setTradeStageAction = (number) => {
  return {
    type: SET_TRADE_STAGE,
    payload: number
  };
};

/**
This action stores the supplied player to application state as the offerd player
in a trade.
*/

export const setOfferedPlayerAction = (playerObject) => {
  return {
    type: SET_OFFERED_PLAYER,
    payload: playerObject
  };
};

/**
This action stores the supplied player to application state as the requested player
in a trade.
*/

export const setRequestedPlayerAction = (playerObject) => {
  return {
    type: SET_REQUESTED_PLAYER,
    payload: playerObject
  };
};

/**
This action stores the given username to application state.
*/

export const storeUserBackupAction = (username) => {
  return {
    type: STORE_USER_BACKUP,
    payload: username
  };
}

/**
This action stores the supplied array of leagues to application state.
*/

export const setUserLeaguesAction = (array) => {
  return {
    type: SET_USER_LEAGUES,
    payload: array
  };
}

/**
This action is used to indicate the whic 'flow' the application is in, which is
used to conditionally render certain screens.
*/

export const flowSetAction = (text) => {
  return {
    type: FLOW_SET,
    payload: text
  };
};
