/**
author: Elliot Gordon

This file exists so that references could be used rather than strings, which
avoided spelling mistakes during implementation.
*/

export const EMAIL_CHANGED = 'email_changed';

export const PASSWORD_CHANGED = 'password_changed';

export const LOGIN_USER_SUCCESS = 'login_user_success';

export const LOGIN_USER_FAIL = 'login_user_fail';

export const LOGIN_USER = 'login_user';

export const CREATE_ACCOUNT = 'create_user';

export const STORE_USER = 'store_user';

export const FB_USER_CREATE = 'fb_user_create';

export const USERNAME_CHANGED = 'username_changed';

export const PLAYER_ID_STORE = 'player_id_store';

export const LEAGUE_SELECTED = 'league_selected';

export const LEAGUE_NAME_SET = 'league_name_set';

export const TARGETED_USER = 'targeted_user';

export const SET_TRADE_VIEW = 'set_trade_view';

export const SET_TRADE_STAGE = 'set_trade_stage';

export const SET_OFFERED_PLAYER = 'set_offered_player';

export const SET_REQUESTED_PLAYER = 'set_requested_player';

export const STORE_USER_BACKUP = 'store_user_backup';

export const SET_USER_LEAGUES = 'set_user_leagues';

export const TIMER_STARTED = 'timer_started';

export const DELETING_LEAGUE = 'deleting_league';

export const ERROR_SET = 'error_set';

export const VIEW_ONLY = 'view_only';

export const FLOW_SET = 'flow_set';
