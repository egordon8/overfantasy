import firebase from 'firebase';
import * as firestore from 'firebase/firestore';
import { Actions } from 'react-native-router-flux';
import {
  PLAYER_ID_STORE
} from './types';

//author: Elliot Gordon


/**
This action stores the selected player ID to application state.
*/

export const playerIDstoreAction = (text) => {
  return {
    type: PLAYER_ID_STORE,
    payload: text
  };
};
