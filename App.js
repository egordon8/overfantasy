import React from 'react';
import firebase from 'firebase';
import * as firestore from 'firebase/firestore';
import { StyleSheet, View, FlatList, TextInput, Button, Text } from 'react-native';
import Router from './src/components/Router';
import CreateLeague from './src/components/Screens/CreateLeague';
import ReduxLoginForm from './src/components/ReduxLoginForm';
import ReduxThunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from './src/redux/reducers';

/**
author: Elliot Gordon

This file is where the application is initialised and runs. The first thing it
does is initialise the connection to firebase.
*/

export default class App extends React.Component {

  constructor(props) {
  super(props);
  this.state = {
    loading: true
  };

  //Firebase configuration details
   this.fbConfig = {
    apiKey: 'AIzaSyAURxap0Tqw0UfimORv8zBNF6lLmRX5HY0',
    authDomain: 'fir-runningproj.firebaseapp.com',
    databaseURL: 'https://fir-runningproj.firebaseio.com',
    projectId: 'fir-runningproj',
    storageBucket: 'fir-runningproj.appspot.com',
    messagingSenderId: '163775465880',
    appId: '1:163775465880:web:851d5ff4042e6b33'
  };
  if (!firebase.apps.length) {
    firebase.initializeApp(this.fbConfig);
  }
}

/**
When the component loads, it initialises the connection to firebase (and firestore)
*/

componentWillMount() {
  if (!firebase.apps.length) {
    firebase.initializeApp(this.fbConfig);
  }
}

/**
The value returned by this method is what is displayed on the screen.
*/

render() {
  return (
    <Provider store={createStore(reducers, {}, applyMiddleware(ReduxThunk))}>

      <Router />

    </Provider>
  );
}

}
