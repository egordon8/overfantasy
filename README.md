This repository contains only the source code for my application as it was too large to store here. 
Instead, the installable APK file is accessible through a google drive link to the download here: https://drive.google.com/open?id=1e8syX2l4KfbpOhBPR-Z78jnLbuCddnk1
In this README file I will explain how to run the application through either the source code or with the APK.

APK

To install the APK, you must use an Android phone running Android 8.0.0 or later.
1 - First, download the APK file. It is called 'OverFantasy1942434.apk'.
2 - Second, turn on developer options if not already enabled. 
    Do this by finding your phone's build number. Its location will vary by device
    but will generally be in settings > software information.
    Tap your build number seven times, and press ok to enable developer mode if 
    prompted.
3 - Now navigate to developer options in settings.
    Enable USB debugging.
4 - Now, using a USB cable, connect your phone to your computer/laptop where you
    have downloaded the APk.
        Open your phone's storage.
        Copy the APK file onto your phone into an accessible folder such as 
        downloads.
        You may now disconnect the phone from the computer.
5 - On your phone, open your files and find the APK.
    Tap on it, and it should give you the option to install it.
    If it does not, then go to your phone's settings and to security settings.
    Enable downloads from unknown sources and attempt to install the APk again.
6 - The app will have been installed and it is now ready to use.

SOURCE CODE

The source code can be run through the Expo CLI.
WINDOWS
    When using the command line below, please make sure you have opened it as an 
    administrator by right-clicking the Command Prompt program and clicking 
    Run as Administrator.
MAC
    When using Mac, preface the below instructions with sudo to allow for 
    administrator access.
1 - Download the source code. You may want to view it using a code editor such
    as Atom or Sublime Text. The name of the root file is 'attempt'.
2 - Go to Node.js and download the latest Node version.
3 - Get the Expo command-line interface by executing the following in the 
    command line or terminal:
        npm install expo-cli --global
4 - Navigate into the project directory.
5 - Perform the following package installations:
        npm install --save react-native-router-flux
        npm install --save react-native-firebase
        npm install --save react-redux
        npm install redux-thunk
6 - On an Android phone, download the Expo application from the Google Play store.
7 - While in the project directory (the 'attempt' folder), type:
        expo start
    A page will pop-up in a web browser, and a QR code will be visible on both
    the web page and in your terminal.
8 - Open the Expo app on your phone, and go to the scan QR code option.
9 - Ensure that your phone and computer/laptop are connected to the same WIFI
    connection.
10 - Use the QR scanner to scan the QR code in either the terminal window or on
    the webpage.
        If there is a blue loading symbol on a white background for a while, 
        followed by a failure to connect, this is a problem with the internet,
        usually meaning that the two devices are not on the same WIFI.
11 - The JavaScript bundle will be built, this may take a few minutes the first
    time, and then the Login screen will appear, indicating that the application
    is ready to use.
        While running through source code, a yellow warning will regularly 
        pop-up. It does not impact the ability of the app to run, so feel free
        to dismiss them.
        